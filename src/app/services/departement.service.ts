import {Injectable} from '@angular/core';
import {Departement} from '../models/Departement';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ServeurService} from './serveur.service';
import {Filiere} from '../models/filiere';
import {Media} from '../models/media';

@Injectable()
export class DepartementService {
  baseURL: string;
  depts: Departement[];
  filieres: Filiere[];
  constructor(private http: HttpClient, private serveurService: ServeurService) {
    this.baseURL = this.serveurService.getBaseUrl();
  }
  getDepartements(): Observable<Departement[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/departements', {headers: httpHeaders});
  }

  getDepartementById(id: number): Observable<Departement> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/departements/' + id, {headers: httpHeaders});
  }
  createDepartement(d: Departement): Observable<Departement> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + '/departements', d, {headers: httpHeaders});
  }
  editDepartement(d: Departement): Observable<Departement> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/departements/' + d.id, d, {headers: httpHeaders});
  }
  deleteDepartement(d: Departement): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + '/departements/' + d.id, {headers: httpHeaders, responseType: 'text'});
  }

  getFilieresByDepartement(idDepartement: number): Observable<Filiere[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/departements/' + idDepartement + '/filieres', {headers: httpHeaders});
  }
  getMediasByDepartement(idDepartement: number): Observable<Media[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/departements/' + idDepartement + '/medias', {headers: httpHeaders});
  }
}

