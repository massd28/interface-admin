import {Injectable} from '@angular/core';
import {Tag} from '../models/Tag';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ServeurService} from './serveur.service';

@Injectable()
export class TagService {
  baseURL: string;
  tags: Tag[];
  constructor(private http: HttpClient, private serveurService: ServeurService) {
    this.baseURL = this.serveurService.getBaseUrl();
  }
  getTags(): Observable<Tag[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/tags', {headers: httpHeaders});
  }
  getTagById(id: number): Observable<Tag> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/tags/' + id, {headers: httpHeaders});
  }
  createTag(t: Tag): Observable<Tag> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + '/tags', t, {headers: httpHeaders});
  }
  editTag(t: Tag): Observable<Tag> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/tags/' + t.id, t, {headers: httpHeaders});
  }
  deleteTag(t: Tag): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + '/tags/' + t.id, {headers: httpHeaders, responseType: 'text'});
  }
}

