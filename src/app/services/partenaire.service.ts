import {Injectable} from '@angular/core';
import {Partenaire} from '../models/Partenaire';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ServeurService} from './serveur.service';

@Injectable()
export class PartenaireService {
  baseURL: string;
  parts: Partenaire[];
  constructor(private http: HttpClient, private serveurService: ServeurService) {
    this.baseURL = this.serveurService.getBaseUrl();
  }
  getPartenaires(): Observable<Partenaire[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/partenaires', {headers: httpHeaders});
  }
  getPartenaireById(id: number): Observable<Partenaire> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/partenaires/' + id, {headers: httpHeaders});
  }
  createPartenaire(p: Partenaire): Observable<Partenaire> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + '/partenaires', p, {headers: httpHeaders});
  }
  editPartenaire(p: Partenaire): Observable<Partenaire> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/partenaires/' + p.id, p, {headers: httpHeaders});
  }
  deletePartenaire(p: Partenaire): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + '/partenaires/' + p.id, {headers: httpHeaders, responseType: 'text'});
  }
}

