import {Injectable} from '@angular/core';
import {AppelDOffres} from '../models/appel-d-offres';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ServeurService} from './serveur.service';
import 'rxjs/Rx';

@Injectable()
export class AppelDOffresService {
  baseURL: string;
  appelDOffres: AppelDOffres[];
  constructor(private http: HttpClient, private serveurService: ServeurService) {
    this.baseURL = this.serveurService.getBaseUrl();
  }
  getAppelDOffres(): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/appelDOffres', {headers: httpHeaders});

  }
  getAppelDOffresById(id: number): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/appelDOffres/' + id, {headers: httpHeaders});

  }
  createAppelDOffres(a: AppelDOffres): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + '/appelDOffres', a, {headers: httpHeaders});

  }
  editAppelDOffres(a: AppelDOffres): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/appelDOffres/' + a.id, a, {headers: httpHeaders});

  }
  deleteAppelDOffres(a: AppelDOffres): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + '/appelDOffres/' + a.id, {headers: httpHeaders, responseType: 'text'});

  }

  getMediasByAppelDOffres(id: number): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/appelDOffres/' + id + '/medias', {headers: httpHeaders});
  }
}
