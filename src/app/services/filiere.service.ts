import {Injectable} from '@angular/core';
import {Filiere} from '../models/Filiere';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ServeurService} from './serveur.service';

@Injectable()
export class FiliereService {
  baseURL: string;
  constructor(private http: HttpClient, private serveurService: ServeurService) {
    this.baseURL = this.serveurService.getBaseUrl();
  }
  getFilieres(): Observable<Filiere[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/filieres', {headers: httpHeaders});
  }
  getFiliereById(id: number): Observable<Filiere> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/filieres/' + id, {headers: httpHeaders});
  }
  createFiliere(f: Filiere): Observable<Filiere> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + '/filieres', f, {headers: httpHeaders});
  }
  editFiliere(f: Filiere): Observable<Filiere> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/filieres/' + f.id, f, {headers: httpHeaders});
  }
  deleteFiliere(f: Filiere): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + '/filieres/' + f.id, {headers: httpHeaders, responseType: 'text'});
  }
}

