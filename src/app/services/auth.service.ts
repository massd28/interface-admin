import { Injectable } from '@angular/core';
import { User } from '../models/user';

@Injectable()
export class AuthService {

  constructor() { }

  logout(): void {
    localStorage.setItem('isLoggedIn', "false");
    localStorage.removeItem('token');
  }

}
