import {Injectable} from '@angular/core';

@Injectable()
export class ServeurRecrutementService {

 //private _serverName = 'http://192.168.1.14';
  private _serverName = 'http://localhost';
  private _serverPort = '8080';


  get serverName(): string {
    return this._serverName;
  }

  get serverPort(): string {
    return this._serverPort;
  }
  getBaseUrl(): string {
    return this._serverName + ':' + this._serverPort;
  }
}
