import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Candidat} from '../../models/models-recrutement/candidat';
import {ServeurRecrutementService} from './serveur-recrutement.service';


@Injectable()
export class CandidatService {
  baseURL: string;
  candidats: Candidat[];
  constructor(private http: HttpClient, private serveurRecrutementService: ServeurRecrutementService) {
    this.baseURL = this.serveurRecrutementService.getBaseUrl();
  }
  getCandidats(): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/candidats', {headers: httpHeaders});
  }
  getCandidatById(id: number): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/candidats/' + id, {headers: httpHeaders});
  }
  createCandidat(c: Candidat): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + '/candidats', c, {headers: httpHeaders});
  }
  editCandidat(c: Candidat): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/candidats/' + c.id, c, {headers: httpHeaders});
  }
  deleteCandidat(c: Candidat): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + '/candidats/' + c.id, {headers: httpHeaders, responseType: 'text'});
  }
  logCandidat(c: Candidat): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + '/login', c, {headers: httpHeaders});
  }
}
