import {HttpClient, HttpHeaders} from '@angular/common/http';
import {SessionRecrutement} from '../../models/models-recrutement/session-recrutement';
import {Injectable} from '@angular/core';
import {ServeurRecrutementService} from './serveur-recrutement.service';
import {Observable} from 'rxjs/Observable';
import {Poste} from '../../models/models-recrutement/poste';
import {SessionPoste} from '../../models/models-recrutement/session-poste';
import {DocumentAFournir} from '../../models/models-recrutement/document-a-fournir';

@Injectable()
export class SessionRecrutementService {
  baseURL: string;
  sessionRecrutements: SessionRecrutement[];
  constructor(private http: HttpClient, private serveurRecrutementService: ServeurRecrutementService) {
    this.baseURL = this.serveurRecrutementService.getBaseUrl();
  }
  getSessionRecrutements(): Observable<SessionRecrutement[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/sessionRecrutement', {headers: httpHeaders});
  }
  getSessionEnCours(): Observable<SessionRecrutement[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/sessionRecrutement/en-cours', {headers: httpHeaders});
  }

  getSessionRecrutementById(id: number): Observable<SessionRecrutement> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/sessionRecrutement/' + id, {headers: httpHeaders});
  }
  getPostesBySession(p: SessionRecrutement): Observable<SessionPoste[]> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/sessionRecrutement/poste/' + p.id, {headers: httpHeaders});
  }
  createSessionRecrutement(d: SessionRecrutement): Observable<SessionRecrutement> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + '/sessionRecrutement', d, {headers: httpHeaders});
  }
  editSessionRecrutement(d: SessionRecrutement): Observable<SessionRecrutement> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/sessionRecrutement', d, {headers: httpHeaders});
  }
  createSessionPoste(d: SessionPoste): Observable<SessionPoste> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + '/sessionRecrutement/sessionPoste-add', d, {headers: httpHeaders});
  }
  editSessionPoste(d: SessionPoste): Observable<SessionPoste> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/sessionRecrutement/sessionPoste', d, {headers: httpHeaders});
  }
  deleteSessionRecrutement(d: SessionRecrutement): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + '/sessionRecrutement/' + d.id, {headers: httpHeaders, responseType: 'text'});
  }
  deleteDoc(d: DocumentAFournir): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + '/sessionRecrutement/sessionPoste/poste/' + d.id, {headers: httpHeaders, responseType: 'text'});
  }
  deleteSessionPoste(s: SessionRecrutement,p: Poste): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + '/sessionRecrutement/' + s.id + '/sessionPoste/poste/' + p.id , {headers: httpHeaders, responseType: 'text'});
  }
}
