import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ServeurRecrutementService} from './serveur-recrutement.service';
import {Injectable} from '@angular/core';
import {Commentaire} from '../../models/models-recrutement/commentaire';

@Injectable()
export class CommentaireService {
  baseURL: string;
  Commentaires: Commentaire[];
  constructor(private http: HttpClient, private serveurRecrutementService: ServeurRecrutementService) {
    this.baseURL = this.serveurRecrutementService.getBaseUrl();
  }
  getCommentaires(): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/Commentaires', {headers: httpHeaders});
  }
  getCommentaireById(id: number): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/Commentaires/' + id, {headers: httpHeaders});
  }
  createCommentaire(c: Commentaire): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + '/Commentaires', c, {headers: httpHeaders});
  }
  editCommentaire(c: Commentaire): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/Commentaires/' + c.id, c, {headers: httpHeaders});
  }
  deleteCommentaire(c: Commentaire): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + '/Commentaires/' + c.id, {headers: httpHeaders, responseType: 'text'});
  }
}
