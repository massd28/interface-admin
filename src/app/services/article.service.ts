
  import {Injectable} from '@angular/core';
import {Article} from '../models/Article';
import {Media} from '../models/Media';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {ServeurService} from './serveur.service';
import {Observable} from 'rxjs';
  import {Partenaire} from '../models/partenaire';

@Injectable()
export class ArticleService {
  baseURL: string;
  articles: Article[];
  constructor(private http: HttpClient, private serveurService: ServeurService) {
    this.baseURL = this.serveurService.getBaseUrl();
  }
  getArticles(): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/articles', {headers: httpHeaders});
  }
  getNews(): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/articles/news', {headers: httpHeaders});
  }
  getArticleById(id: number): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/articles/' + id, {headers: httpHeaders});
  }
  createArticle(a: Article): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + '/articles', a, {headers: httpHeaders});
  }
  editArticle(a: Article): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/articles/' + a.id, a, {headers: httpHeaders});
  }
  publierArticle(a: Article): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/articles/' + a.id, a, {headers: httpHeaders});
  }
  deleteArticle(a: Article): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + '/articles/' + a.id, {headers: httpHeaders, responseType: 'text'});
  }
  deleteMedia(m: Media, a: Article): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.delete(this.baseURL + '/articles/' + a.id + '/suppression-media/' + m.id, {headers: httpHeaders, responseType: 'text'});
  }

  getArticlesByTag(nomTag: string): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/articles' + nomTag, {headers: httpHeaders});
  }
  getArticleByMediaId(idMedia: number): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/articles' + idMedia, {headers: httpHeaders});
  }
  getMediasByArticle(id: number): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/articles/' + id + '/medias', {headers: httpHeaders});
  }
  getArticlesByPage(page: string): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/articles/pagescibles/' + page, {headers: httpHeaders});
  }
  getArticlesByPartenaire(part: Partenaire): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/articles/partenaires/' + part.id, {headers: httpHeaders});
  }
}
