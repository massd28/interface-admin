import {Injectable} from '@angular/core';
import {AppelDOffres} from '../models/appel-d-offres';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ServeurService} from './serveur.service';
import 'rxjs/Rx';
import {Allocution} from '../models/allocution';
import {Article} from '../models/article';

@Injectable()
export class AllocutionService {
  baseURL: string;
  allocutions: Allocution[];
  constructor(private http: HttpClient, private serveurService: ServeurService) {
    this.baseURL = this.serveurService.getBaseUrl();
  }
  getAllocutions(): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/allocutions', {headers: httpHeaders});

  }

  getRandomAllocution(): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/allocution', {headers: httpHeaders});

  }


  getAllocutionById(id: number): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.get(this.baseURL + '/allocutions/' + id, {headers: httpHeaders});

  }
  editAllocution(a: Allocution): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.put(this.baseURL + '/articles/' + a.id, a, {headers: httpHeaders});
  }
  saveAllocution(a: Allocution ): any {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('Accept', 'application/json');
    return this.http.post(this.baseURL + '/allocutions', a, {headers: httpHeaders});

  }
}
