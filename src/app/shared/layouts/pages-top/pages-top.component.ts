import { Component, Input } from '@angular/core';
import { GlobalService } from '../../services/global.service';
import {PartenaireService} from '../../../services/partenaire.service';
import {ArticleService} from '../../../services/article.service';
import {Article} from '../../../models/article';
import {Partenaire} from '../../../models/partenaire';
import {MessageService} from '../../../services/message.service';
import {Message} from '../../../models/message';
import {Router} from '@angular/router';

@Component({
  selector: 'pages-top',
  templateUrl: './pages-top.component.html',
  styleUrls: ['./pages-top.component.scss'],
  providers: [MessageService, ArticleService, PartenaireService]
})
export class PagesTopComponent {
  avatarImgSrc: string = 'assets/images/avatar.jpg';
  userName: string = 'Espace Admin';
  userPost: string = 'ISEP de Diamniadio';


  sidebarToggle: boolean = true;
  tip = { ring: false, email: false };
  articles: Article[];
  parts: Partenaire[];
  messagesNonLus: Message[];
  constructor(private _globalService: GlobalService,
              private articleService: ArticleService,
              private partService: PartenaireService,
              private messService: MessageService,
              private router: Router) {
    this.articles = [];
    this.messagesNonLus = [];
    this.messService.getMessagesNonLu().subscribe(
      m => {
        this.messagesNonLus = m;
      },
      err => {console.log(err);}
    );
    this.articleService.getArticlesByPage('eco').subscribe(
      articles => {
        this.articles = articles;

      },err => {console.log(err);}
    );
    this.parts = [];
    this.partService.getPartenaires().subscribe(
      parts => {
        this.parts = parts;
      }, err => {console.log(err);}
    )
  }

  public _sidebarToggle() {
    /* this._globalService.sidebarToggle$.subscribe(sidebarToggle => {
      this.sidebarToggle = sidebarToggle;
    }, error => {
      console.log('Error: ' + error);
    }); */

    this._globalService.data$.subscribe(data => {
      if (data.ev === 'sidebarToggle') {
        this.sidebarToggle = data.value;
      }
    }, error => {
      console.log('Error: ' + error);
    });
    this._globalService.dataBusChanged('sidebarToggle', !this.sidebarToggle);


    //this._globalService._sidebarToggleState(!this.sidebarToggle);
  }

  onGoToMessagerie () {
    this.router.navigate(['pages/espace-admin/messagerie']);
  }
}
