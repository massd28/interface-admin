import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import {JsonpModule} from '@angular/http';
/* components */
import {CardComponent} from './components/card/card.component';
import {TodolistComponent} from './components/todolist/todolist.component';
import {TabsetComponent} from './components/tabset/tabset.component';
import {TabContentComponent} from './components/tabset/tab-content/tab-content.component';
import {ProgressBarComponent} from './components/progress-bar/progress-bar.component';
import {FileTreeComponent} from './components/file-tree/file-tree.component';
import {SwitchComponent} from './components/switch/switch.component';
import {PellEditorComponent} from './components/pell-editor/pell-editor.component';
import {AlertComponent} from './components/alert/alert.component';
import {WeatherComponent} from './components/weather/weather.component';
import {ProfileComponent} from './components/profile/profile.component';
import {ReadArticleComponent} from '../pages/read-article/read-article.component';
import {HttpClientModule} from '@angular/common/http';
import {ServeurService} from '../services/serveur.service';
import {EditorComponent} from '../pages/editor/editor.component';
import {SelectModule} from 'ng2-select';
import {EditorArticleComponent} from '../pages/editor-article/editor-article.component';
import {ArticlesComponent} from '../pages/articles/articles.component';
import {AppelDOffresComponent} from '../pages/appel-doffres/appel-doffres.component';
import {EventsComponent} from '../pages/events/events.component';
import {ShowArticleComponent} from '../pages/show-article/show-article.component';
import {FileUploadModule} from 'ng2-file-upload';
import {FileUploadComponent} from '../pages/form/components/file-upload/file-upload.component';
import {Ng2SelectComponent} from '../pages/form/components/ng2-select/ng2-select.component';
import {RouterModule} from '@angular/router';
import {SingleSelectComponent} from '../pages/form/components/ng2-select/single-select/single-select.component';
import {MultipleSelectComponent} from '../pages/editor-article/multiple-select/multiple-select.component';
import {AddUserComponent} from '../pages/add-user/add-user.component';
import {EcosystemesComponent} from '../pages/ecosystemes/ecosystemes.component';
import {AllocutionComponent} from '../pages/allocution/allocution.component';
import {EditorAllocutionComponent} from '../pages/allocution/editor-allocution/editor-allocution.component';
import {PrevComponent} from '../pages/prev/prev.component';
import {DateTimePickerModule} from 'ng-pick-datetime';
import {Ng2SearchPipeModule} from 'ng2-search-filter';
import {ModalModule} from 'ngx-modal';
import {ModalsComponent} from '../pages/ui/components/modals/modals.component';
import {MediasComponent} from '../pages/medias/medias.component';
import {EditorMessageComponent} from '../pages/espace-admin/editor-message/editor-message.component';
import {MessagesSentComponent} from '../pages/espace-admin/messages-sent/messages-sent.component';
import {EspaceAdminComponent} from '../pages/espace-admin/espace-admin.component';
import {ReponseMessageComponent} from '../pages/espace-admin/reponse-message/reponse-message.component';
import {MessagerieComponent} from '../pages/espace-admin/messagerie/messagerie.component';
import {ShowMessageComponent} from '../pages/espace-admin/show-message/show-message.component';
import {LabelsComponent} from '../pages/labels/labels.component';
import {EditorLabelsComponent} from '../pages/labels/editor-labels/editor-labels.component';
import {NgxSmartModalModule, NgxSmartModalService} from 'ngx-smart-modal';
import {ServeurRecrutementService} from '../services/recrutement-services/serveur-recrutement.service';


@NgModule({
  imports: [
    CommonModule,
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    JsonpModule,
    HttpClientModule,
    SelectModule,
    ModalModule,
    FileUploadModule,
    DateTimePickerModule,
    RouterModule,
    Ng2SearchPipeModule,
    NgxSmartModalModule

  ],
  declarations: [
    CardComponent,
    FileUploadComponent,
    Ng2SelectComponent,
    FileTreeComponent,
    TodolistComponent,
    TabsetComponent,
    TabContentComponent,
    ProgressBarComponent,
    SwitchComponent,
    PellEditorComponent,
    AlertComponent,
    WeatherComponent,
    ProfileComponent,
    ReadArticleComponent,
    EditorComponent,
    //EditorEventsComponent,
    EditorArticleComponent,
    //EditorAppelDoffresComponent,
    ArticlesComponent,
    AppelDOffresComponent,
    EventsComponent,
    ShowArticleComponent,
    ModalsComponent,
    SingleSelectComponent,
    MultipleSelectComponent,
    AddUserComponent,
    EcosystemesComponent,
    AllocutionComponent,
    EditorAllocutionComponent,
    PrevComponent,
    //ShoweventsComponent,
    //ShowappelComponent,
    MediasComponent,
    EspaceAdminComponent,
    MessagerieComponent,
    ShowMessageComponent,
    EditorMessageComponent,
    MessagesSentComponent,
    ReponseMessageComponent,
    LabelsComponent,
    EditorLabelsComponent,


  ],
  exports: [
    CardComponent,
    FileTreeComponent,
    TodolistComponent,
    TabsetComponent,
    TabContentComponent,
    ProgressBarComponent,
    SwitchComponent,
    PellEditorComponent,
    AlertComponent,
    WeatherComponent,
    ProfileComponent
  ],
  providers:[
    ServeurService,
    NgxSmartModalService,
    ServeurRecrutementService
  ]
})
export class SharedModule { }
