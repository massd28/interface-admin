import {User} from './user';
import {Media} from './media';
import {Article} from './article';

export class Tag {
  id: number;
  nom: string;
  articles: Article[];
}

