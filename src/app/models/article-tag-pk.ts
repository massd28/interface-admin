import {Article} from './article';
import {Tag} from './tag';

export class ArticleTagPk {
  article: Article;
  tag: Tag;
}
