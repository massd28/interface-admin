import {Partenaire} from './partenaire';
import {Departement} from './departement';
import {Filiere} from './filiere';

export class User {
  id: number;
  prenom: string;
  nom: string;
  mail: string;
  adress: string;
  username: string;
  password: string;
  telephone: string;
  role: string;
  filieres: Filiere[];
  departements: Departement[];
  partenaires: Partenaire[];

}
