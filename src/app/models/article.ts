import {ArticleTag} from './article-tag';
import {Media} from './media';
import {MediaArticle} from './media-article';
import {Partenaire} from './partenaire';


export class Article {
  id: number;
  titre: string;
  corps: string;
  pageCible: string;
  dateCreation: Date;
  datePublication: Date;
  state: boolean;
  news: boolean;
  articleTags: ArticleTag[];
  media: Media;
  images: Media[];
  videos: Media[];
  documents: Media[];
  mediaArticles: MediaArticle[];
  partenaire: Partenaire;
}
