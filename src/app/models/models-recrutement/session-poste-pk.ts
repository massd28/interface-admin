import {Poste} from './poste';
import {SessionRecrutement} from './session-recrutement';

export class SessionPostePk {
  poste: Poste;
  sessionRecrutement: SessionRecrutement;
}
