import {DossierCandidature} from './dossier-candidature';

export class Candidat {
  id: number;
  prenom: string;
  nom: string;
  email: string;
  username: string;
  password: string;
  dossierCandidatures:DossierCandidature[] ;
}
