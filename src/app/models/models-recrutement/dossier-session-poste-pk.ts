import {DossierCandidature} from './dossier-candidature';
import {SessionPoste} from './session-poste';

export class DossierSessionPostePk {
  dossierCandidature: DossierCandidature;
  poste: SessionPoste;
}
