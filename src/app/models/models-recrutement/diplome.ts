import {DossierCandidature} from './dossier-candidature';

export class Diplome {
  id: number;
  nom: string;
  anneeObtention: string;
  structure: string;
  dossierCandidature: DossierCandidature;
}
