import {SessionPostePk} from './session-poste-pk';
import {DocumentAFournir} from './document-a-fournir';

export class SessionPoste {
  dateCreation: Date;
  pk: SessionPostePk;
  documentAFournirs: DocumentAFournir[];
}
