import {Departement} from './departement';
import {User} from './user';

export class Filiere {
  id: number;
  nom: string;
 description: string;
  presentation: string;
  objectif: string;
  conditionAdmission: string;
  debouches: string;
  departement: Departement;
  user: User;
}
