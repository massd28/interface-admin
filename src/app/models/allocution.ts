
import {Media} from './media';


export class Allocution {
  id: number;
  titre: string;
  corps: string;
  media: Media;
}
