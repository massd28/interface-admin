import {User} from './user';
import {Media} from './media';
import {Article} from './article';

export class Partenaire {
  id: number;
  nom: string;
  siteWeb: string;
  media: Media;
  user: User;
  descriptionPartenariat: string;
  articles: Article[];
}
