import {Filiere} from './filiere';
import {User} from './user';
import {Media} from './media';

export class Departement {
  id: number;
  nom: string;
  description: string;
  filieres: Filiere[];
  user: User;
  medias: Media[];
}
