import {Article} from './article';

export class Event extends Article {
  dateDebut: Date;
  dateFin: Date;
  lieu: string;
  type: string;
}
