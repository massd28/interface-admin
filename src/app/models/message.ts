import {Media} from './media';

export class Message {
  id: number;
  objet: String;
  corps: String;
  nameEmetteur: String;
  emailEmetteur: String;
  emailDestinataire: String;
  phoneEmetteur: String;
  companyEmetteur: String;
  dateEnvoi: Date;
  received: boolean;
  sent: boolean;
  lu: boolean;


}
