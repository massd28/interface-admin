import {Article} from './article';
import {MediaArticle} from './media-article';

export class Media {
  id: number;
  nom: string;
  url: string;
  type: string;
  articles: Article[];
  mediaArticles: MediaArticle[];
}
