import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Label} from '../../models/label';
import {LabelService} from '../../services/label.service';

@Component({
  selector: 'app-labels',
  templateUrl: './labels.component.html',
  styleUrls: ['./labels.component.scss'],
  providers: [LabelService]
})
export class LabelsComponent implements OnInit {

  formLabel: FormGroup;
  @Output() back: EventEmitter<Label> = new EventEmitter();
  l: Label;
  message: string;
  labels: Label[];
  label: Label;
  deleting: boolean;
  gettingLabels: boolean;
  feedBack(lab: Label): void {
    this.labels.push(lab);
  }

  ngOnInit(): void {
    this.label = new Label();
    this.getLabels();


  }


  onDeleteLabel(label: Label): void {
    this.deleting = true;
    this.labelService.deleteLabel(label).subscribe(
      () => {
        for (let i = 0; i < this.labels.length; i++) {
          if (this.labels[i].id === label.id) {
            this.labels.splice(i, 1);
            break;
          }
        }
        this.deleting = false;
      },
      err => {
        console.log(err);
      }
    );
  }

  constructor(private fB: FormBuilder, private router: Router,
              private labelService: LabelService) {
    this.formLabel = this.fB.group({
      'titre': ['', Validators.required],
      'corps': ['', Validators.required],

    });
    this.l = new Label();


  }



  onSubmit() {
    if (this.l.id === undefined) {
      this.labelService.createLabel(this.l).subscribe(
        label => {
          this.back.emit(label);
          this.message = 'Successfully created!';

          this.router.navigate(['pages/labels']);
          setTimeout(
            () => this.message = undefined,
            2000
          );
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.labelService.editLabel(this.l).subscribe(
        label => {
          this.back.emit(label);
          this.message = 'Successfully updated!';
          this.router.navigate(['pages/labels']);
          setTimeout(
            () => this.message = undefined,
            2000
          );
        },
        error => {
          console.log(error);
        }
      );

    }
  }

  getLabels() {
    this.gettingLabels = true;
    this.labelService.getLabels().subscribe(
      labels => {
        this.labels = labels;
        console.log(this.labels);
        this.gettingLabels = false;
      },
      error => {
        console.log(error);
      }
    );
  }
  goToEditor(){
    this.router.navigate(['pages/editor-labels']);
  }

}

