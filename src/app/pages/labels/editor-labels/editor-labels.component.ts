import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {LabelService} from '../../../services/label.service';
import {Label} from '../../../models/label';


@Component({
  selector: 'app-editor-labels',
  templateUrl: './editor-labels.component.html',
  styleUrls: ['./editor-labels.component.scss'],
  providers: [LabelService]
})
export class EditorLabelsComponent implements OnInit {



  formLabel: FormGroup;
  @Output() back: EventEmitter<Label> = new EventEmitter();
  l: Label;
  message: string;
  labels: Label[];
  label: Label;

  feedBack(lab: Label): void {
    this.labels.push(lab);
  }

  ngOnInit(): void {
    this.label = new Label();
    this.getLabels();


  }


  onDeleteLabel(label: Label): void {
    this.labelService.deleteLabel(label).subscribe(
      () => {
        for (let i = 0; i < this.labels.length; i++) {
          if (this.labels[i].id === label.id) {
            this.labels.splice(i, 1);
            break;
          }
        }
      }
    );
  }

  constructor(private fB: FormBuilder, private router: Router,
              private labelService: LabelService) {
    this.formLabel = this.fB.group({
      'titre': ['', Validators.required],
      'corps': ['', Validators.required],

    });
    this.l = new Label();


  }



  onSubmit() {
    if (this.l.id === undefined) {
      this.labelService.createLabel(this.l).subscribe(
        label => {
          this.back.emit(label);
          this.message = 'Successfully created!';

          this.router.navigate(['pages/labels']);
          setTimeout(
            () => this.message = undefined,
            2000
          );
        },
        error => {
          this.message = error.message;
          console.log(error.message);
          console.log(error);
        }
      );
    } else {
      this.labelService.editLabel(this.l).subscribe(
        label => {
          this.back.emit(label);
          this.message = 'Successfully updated!';
          this.router.navigate(['pages/labels']);
          setTimeout(
            () => this.message = undefined,
            2000
          );
        },
        error => {
          this.message = error.message;
          console.log(error.message);
          console.log(error);
        }
      );

    }
  }

   getLabels() {
    this.labelService.getLabels().subscribe(
      labels => {
        this.labels = labels;
        console.log(this.labels);
      },
      error => {
        this.message = error.message;
        console.log(error.message);
        console.log(error);
      }
    );
  }

}

