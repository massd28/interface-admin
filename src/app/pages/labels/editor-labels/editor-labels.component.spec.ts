import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorLabelsComponent } from './editor-labels.component';

describe('EditorLabelsComponent', () => {
  let component: EditorLabelsComponent;
  let fixture: ComponentFixture<EditorLabelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorLabelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorLabelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
