import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {UserService} from '../../services/user.service';
import {User} from '../../models/user';
import {Role} from '../../models/role';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [UserService]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  user: User;
  users: User[]=[];
  roles: Role[]=[];
  mess: any;
  messError: any;
  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private userService: UserService) {

  }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      this.messError = 'Données invalides: Réessayez!';
    }
    this.userService.login(this.user).subscribe(
      () =>{
        this.mess = 'Connecté avec succès!';
        setTimeout(
          () => this.mess = undefined,
          2000
        );
        for (let role of this.roles){
          if (this.user.role === role.valeur ) {
            this.router.navigate([''+role.pUrls]);
          }
        }
      },
      error1 => {
        console.log(error1);
        this.messError = error1.errorMessage;
      }
    )
  }
  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }


  ngOnInit() {
   this.roles=[
     {valeur: 'admin', libelle: 'Administrateur', pUrls: 'pages/index'},
     {valeur: 'article', libelle: 'Gestionnaire articles', pUrls: 'pages/editor-article'},
     {valeur: 'annonce', libelle: 'Gestionnaire annonces', pUrls: 'pages/editor-article'},
     {valeur: 'ecosysteme', libelle: 'Administrateur écosystème', pUrls: 'pages/ecosystemes'},
     {valeur: 'events', libelle: 'Gestionnaire évènements', pUrls: 'pages/events'},
   ];
    this.getUsers();
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      role: ['', Validators.required]
    });
    this.user = new User();
    this.user.username = this.f.username.value;
    this.user.password = this.f.password.value;
    this.user.role = this.f.role.value;
  }
  private getUsers () {
    this.userService.getUsers().subscribe(
      users=> {
        this.users = users;
        console.log(this.users);
      },
      error1 => {
        console.log(error1);
        //console.log(error1.error.error.error.messages);
      }
    );
  }

  onRoleChange () {
    console.log('Profile Changed: '+ this.user.role);
  }
}
