import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import {ModalModule} from 'ngx-modal';
import {NgxPaginationModule} from 'ngx-pagination';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {routing} from './ecosystemes.routing';
import {RouterModule} from '@angular/router';
import {EditorPartComponent} from './editor-part/editor-part.component';
import {FileUploadModule} from 'ng2-file-upload';
import {EditorArticleEcoComponent} from './editor-article-eco/editor-article-eco.component';
import {SelectModule} from 'ng2-select';
import {ShowPartenaireComponent} from './show-partenaire/show-partenaire.component';
import {DateTimePickerModule} from 'ng-pick-datetime';
import {NgxSmartModalModule, NgxSmartModalService} from 'ngx-smart-modal';
import {ShowArticleEcoComponent} from './show-article-eco/show-article-eco.component';
import {ErrorComponent} from '../errors/error/error.component';


@NgModule({
  declarations: [
  EditorPartComponent,
  EditorArticleEcoComponent,
  ShowPartenaireComponent,
  ShowArticleEcoComponent,
  ErrorComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ModalModule,
    routing,
    RouterModule,
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    SelectModule,
    DateTimePickerModule,
    NgxSmartModalModule,

  ],
  providers: [
    NgxSmartModalService
  ]
})
export class EcosystemesModule { }
