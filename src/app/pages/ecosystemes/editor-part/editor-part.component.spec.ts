import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorPartComponent } from './editor-part.component';

describe('EditorPartComponent', () => {
  let component: EditorPartComponent;
  let fixture: ComponentFixture<EditorPartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorPartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorPartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
