import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { FileUploader } from 'ng2-file-upload';
import {Media} from '../../../models/media';
import {MediaService} from '../../../services/media.service';
import {PartenaireService} from '../../../services/partenaire.service';
import {Partenaire} from '../../../models/partenaire';
import {Router} from '@angular/router';


const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';
@Component({
  selector: 'app-editor-part',
  templateUrl: './editor-part.component.html',
  styleUrls: ['./editor-part.component.scss'],
  providers: [PartenaireService, MediaService]
})

export class EditorPartComponent implements OnInit, OnChanges {
  public uploader: FileUploader = new FileUploader({url: URL});
  submittingForm: boolean;
  formPart: FormGroup;
  @Input()p: Partenaire;
  @Input()descriptif: string;
  @Output()backPart: EventEmitter<Partenaire> = new EventEmitter();
  m: Media;
  media: Media;
  message: string;
  partenaires: Partenaire[];
  medias: Media[];
  partenaire: Partenaire;
  titre: string;
  casCreation: boolean;

  feedBack(pa: Partenaire): void {
    this.partenaires.push(pa);
  }

  ngOnInit(): void {
  }
  constructor(private fB: FormBuilder,
              private partenaireService: PartenaireService,
              private router: Router,
              private mediaservice: MediaService) {
    this.formPart = this.fB.group({
      'nom': ['', Validators.required],
      'site': [''],

    });
    if(this.p === undefined){
      this.p = new Partenaire();
      this.p.descriptionPartenariat = '';
      this.titre ='Nouveau partenaire';
      this.casCreation = true;
    }else {
      this.titre = 'Modification';
      this.casCreation = false;
    }
    this.medias = [];
    this.m = new Media();
    this.descriptif = this.p.descriptionPartenariat;
  }
  public fileDropped(event: any) {
    const $this = this;
    for(let item of event){
      const media = new Media;
      let reader = new FileReader();
      console.log(item);
      reader.readAsDataURL(item);
      reader.onload = function () {
        media.nom = item.name;
        media.url = reader.result.split(',')[1];
        media.type = item.name.split('.')[item.name.split('.').length - 1];
        $this.medias.push(media);
      };
    }
    console.log(this.medias);
  }
  onUpload() {
    console.log('#############');
    this.submittingForm = true;
    this.mediaservice.createMedia(this.medias).subscribe(
      medias => {
        console.log('************************');
        console.log(medias);
        this.submittingForm = false;
        this.message = 'Logo créé avec succès';
        setTimeout(
          () => this.message = undefined,
          2000
        );
        this.p.media = medias[0];
      },error => {
        this.message = error.message;
        this.router.navigate(['pages/errors/', this.message]);
        console.log(error.message);
        console.log(error);
      }
    );
  }
  onSubmit() {
    if (this.p.id === undefined) {
      this.submittingForm = true;
      this.partenaireService.createPartenaire(this.p).subscribe(
        partenaire => {
          this.backPart.emit(partenaire);
          this.submittingForm = false;
          this.message = 'Partenaire ok';
          setTimeout(
            () => this.message = undefined,
            2000
          );
          this.router.navigate(['pages/ecosystemes']);
        },
        error => {
          this.message = error.message;
          this.router.navigate(['pages/errors/', this.message]);
          console.log(error.message);
          console.log(error);
        }
      );
    } else {
      this.submittingForm = true;
      this.partenaireService.editPartenaire(this.p).subscribe(
        partenaire => {
          this.backPart.emit(partenaire);
          this.submittingForm = false;
          this.message = 'Mise à jour réussie';
          setTimeout(
            () => this.message = undefined,
            2000
          );
          this.router.navigate(['pages/ecosystemes']);
        },
        error => {
          this.message = error.message;
          this.router.navigate(['pages/errors/', this.message]);
          console.log(error.message);
          console.log(error);
        }
      );

    }
  }
  onContentChangePart (event: string) {
    this.p.descriptionPartenariat = event;
    console.log(this.p.descriptionPartenariat);
  }
  ngOnChanges (changes: SimpleChanges): void {
    if (this.p && this.p.descriptionPartenariat) {
      this.descriptif = this.p.descriptionPartenariat;
    }
  }

}

