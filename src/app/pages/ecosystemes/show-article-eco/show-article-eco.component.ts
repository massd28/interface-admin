import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Article} from '../../../models/article';
import {ArticleService} from '../../../services/article.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Media} from '../../../models/media';

@Component({
  selector: 'app-show-article-eco',
  templateUrl: './show-article-eco.component.html',
  styleUrls: ['./show-article-eco.component.scss'],
  providers: [ArticleService]
})
export class ShowArticleEcoComponent implements OnInit {

  article: Article;
  idd: number;
  @Output() back: EventEmitter<Article> = new EventEmitter();
  modifArt = false;
  publierAfter = false;
  fr = {
    firstDayOfWeek: 0,
    dayNames: ['Dimanche', 'Lundi', 'Mardi','Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
    dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
    monthNames: [ 'Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Decembre' ],
    monthNamesShort: [ 'Jan', 'Fév', 'Mars', 'Avr', 'Mai', 'Juin','Juil', 'Août', 'Sep', 'Oct', 'Nov', 'Dec' ]
  };
 // htmlStr: string = '<strong>The Tortoise</strong> &amp; the Hare';
  constructor(private articleService: ArticleService,
              private router: Router,
              private route: ActivatedRoute) {}
  ngOnInit(): void {
    this.article = new Article();
    this.article.images = [];
    this.idd = this.route.snapshot.params['id'];
    this.getArticle();


  }


  onPublier(a: Article) {
    a.state = true;
    a.datePublication = new Date();
    this.articleService.editArticle(a).subscribe(
      article => {
        this.back.emit(article);

        this.router.navigate(['pages/ecosystemes']);
        console.log(a.state);

      }
    );
  }

  getArticle(): void {
    this.articleService.getArticleById(this.route.snapshot.params['id']).subscribe(
      a => {
        a.images = [];
        a.videos = [];
        a.documents = [];
        this.articleService.getMediasByArticle(a.id).subscribe(
          medias => {
            for (let i = 0; i < medias.length; i++) {
              if (medias[i].type === 'png' || medias[i].type === 'jpg' || medias[i].type === 'jpeg') {
                a.images.push(medias[i]);
              }
              if (medias[i].type === 'mp4' || medias[i].type === 'mov' || medias[i].type === 'wav') {
                a.videos.push(medias[i]);
              }
              if (medias[i].type === 'pdf' || medias[i].type === 'txt') {
                a.documents.push(medias[i]);
              }
            }
          }
        );
        this.article = a;
        document.getElementById('texte').innerHTML = a.corps;
        console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
        console.log(this.article.mediaArticles);

      }
    );
  }
  onPublierAfter(){
    this.publierAfter = true;
  }
  onValider(a: Article){
    a.state = true;
    a.datePublication = this.article.datePublication;
    this.articleService.editArticle(a).subscribe(
      article => {
        this.back.emit(article);
        this.router.navigate(['pages/ecosystemes']);
        console.log(a.state);

      }
    );
  }
}
