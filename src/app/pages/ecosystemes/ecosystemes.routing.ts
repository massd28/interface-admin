import { Routes, RouterModule } from '@angular/router';
import {EditorAppelDoffresComponent} from '../editor/components/editor-appel-doffres/editor-appel-doffres.component';
import {EcosystemesComponent} from './ecosystemes.component';
import {EditorPartComponent} from './editor-part/editor-part.component';
import {EditorArticleEcoComponent} from './editor-article-eco/editor-article-eco.component';
import {ShowPartenaireComponent} from './show-partenaire/show-partenaire.component';
import {ShowArticleEcoComponent} from './show-article-eco/show-article-eco.component';




const childRoutes: Routes = [
  {
    path: '',
    component: EcosystemesComponent,

  },

  {
    path: 'editor-part',
    component: EditorPartComponent,

  },
  {
    path: 'editor-article',
    component: EditorArticleEcoComponent
  },
  {
    path: 'show-partenaire/:id',
    component: ShowPartenaireComponent
  },
  {
    path: 'show-article/:id',
    component: ShowArticleEcoComponent
  },

];

export const routing = RouterModule.forChild(childRoutes);
