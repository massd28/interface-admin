import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Partenaire} from '../../../models/partenaire';
import {PartenaireService} from '../../../services/partenaire.service';
import {Article} from '../../../models/article';
import {ArticleService} from '../../../services/article.service';

@Component({
  selector: 'app-show-partenaire',
  templateUrl: './show-partenaire.component.html',
  styleUrls: ['./show-partenaire.component.scss'],
  providers: [PartenaireService, ArticleService]
})
export class ShowPartenaireComponent implements OnInit {
  partenaire: Partenaire;
  articles: Article[];
  partenaires: Partenaire[];
  modifPart = false;
  constructor(private partService: PartenaireService,
              private articleService: ArticleService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.partenaire = new Partenaire();
    this.articles = [];
    this.partenaires = [];
    this.partService.getPartenaireById(parseInt(this.route.snapshot.params['id'])).subscribe(
      part => {
        this.partenaire = part;
        document.getElementById('text').innerHTML = this.partenaire.descriptionPartenariat;
        this.articleService.getArticlesByPartenaire(this.partenaire).subscribe(
          articles => {
            this.articles = articles;
            console.log('####Articles en rapport###');
            console.log(this.articles);
          },
          err => {console.log(err);}
        );
      },
      error1 => {
        console.log(error1);
      }
    );
    this.partService.getPartenaires().subscribe(
      parts => {
        this.partenaires = parts;
        console.log('****Autres partenaires*****');
        console.log(this.partenaires);
      },
      error => {
        console.log(error);
      }
    )
  }

  onVoirAutrePart (p: Partenaire) {
    this.router.navigate(['pages/ecosystemes/show-partenaire/', p.id]);
  }
}
