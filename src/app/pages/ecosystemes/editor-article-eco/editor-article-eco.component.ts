import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FileUploader} from 'ng2-file-upload';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Article} from '../../../models/article';
import {Media} from '../../../models/media';
import {ActivatedRoute, Router} from '@angular/router';
import {ArticleService} from '../../../services/article.service';
import {MediaService} from '../../../services/media.service';
import {MediaArticle} from '../../../models/media-article';
import {MediaArticlePk} from '../../../models/media-article-pk';
import {Partenaire} from '../../../models/partenaire';
import {PartenaireService} from '../../../services/partenaire.service';
import {Tag} from '../../../models/tag';
import {TagService} from '../../../services/tag.service';
import {ArticleTag} from '../../../models/article-tag';
import {ArticleTagPk} from '../../../models/article-tag-pk';
const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
  selector: 'app-editor-article-eco',
  templateUrl: './editor-article-eco.component.html',
  styleUrls: ['./editor-article-eco.component.scss'],
  providers: [ArticleService, MediaService, PartenaireService, TagService]
})
export class EditorArticleEcoComponent implements OnInit, OnChanges {
  public uploader: FileUploader = new FileUploader({url: URL});
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;
  titreAr: string;
  submittingForm: boolean;
  public fileOverBase (e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother (e: any): void {
    this.hasAnotherDropZoneOver = e;
  }


  formArticle: FormGroup;
  @Output() back: EventEmitter<Article> = new EventEmitter();
  @Input() a: Article;
  @Input() defaultContentAr: string;
  m: Media;
  mediaOkk: boolean = false;
  media: Media;
  message: string;
  messageErr: string;
  articles: Article[];
  @Input() medias: Media[];
  article: Article;
  parts: Partenaire[];
  partenaire: Partenaire;
  casCreation: boolean;
  gettingParts: boolean;
  public items: Array<any>;
  tags: Tag[];
  ajoutTag: boolean = false;
  formTag: FormGroup;
  @Output() backTag: EventEmitter<Tag> = new EventEmitter();
  t: Tag;
  feedBack (ar: Article): void {
    this.articles.push(ar);
    this.partenaire = ar.partenaire;
    this.partenaire.articles.push(ar);
  }
  onSubmitTag() {
    if (this.t.id === undefined) {
      this.tagService.createTag(this.t).subscribe(
        tag => {
          this.backTag.emit(tag);

          this.ajoutTag = false;
          const item = {
            id: tag.id,
            text: tag.nom
          }
          this.value.push(item);
        },
        error => {
          console.log(error);
        }
      );

    } else {
      this.tagService.editTag(this.t).subscribe(
        tag => {
          this.backTag.emit(tag);
        },
        error => {
          console.log(error);
        }
      );

    }
  }
  private getTags() {
    this.tagService.getTags().subscribe(
      tags => {
        this.tags = tags;
        for (let t of this.tags){
          const item = {
            id: t.id,
            text: t.nom
          }
          this.items.push(item) ;
        }
        console.log(this.items);
      },
      error => {
        console.log(error);

      }
    );
  }

  ngOnInit (): void {

    this.partenaire = new Partenaire();
    this.partenaire.articles = [];
    this.gettingParts = true;
    this.getTags();
    this.partService.getPartenaires().subscribe(
      parts => {
        this.parts = parts;
        console.log(this.parts);
        this.gettingParts = false;
      },
      error1 => {
        console.log(error1);
      }
    );
    this.formArticle = this.fB.group({
      'titre': ['', Validators.required],
      'partenaire': ['']

    });
    if(this.a === undefined){
      this.casCreation = true;
      console.log('*******************************************');
      this.a = new Article();
      this.a.corps = '';
      this.a.articleTags = [];
      this.a.mediaArticles = [];
      this.titreAr = 'Nouvel article';
    }else {
      this.titreAr = 'Modification';
      this.casCreation = false;
    }
    this.medias = [];
    this.defaultContentAr = this.a.corps;
    this.m = new Media();
  }

  onContentChange (event: string) {
    this.a.corps = event;
    console.log(this.a.corps);
  }

  constructor (private fB: FormBuilder,
               private router: Router,
               private route: ActivatedRoute,
               private articleService: ArticleService,
               private mediaservice: MediaService,
               private partService: PartenaireService,
               private tagService: TagService) {
    this.formTag = this.fB.group({
      'nom': ['', Validators.required],

    });
    this.t = new Tag();
    this.value = [];
    this.items = [];
  }

  ajouterTag(){
    this.ajoutTag = true;
  }
  public fileDropped (event: any) {

    const $this = this;
    for (let item of event) {
      const media = new Media;
      let reader = new FileReader();
      console.log(item);
      reader.readAsDataURL(item);
      reader.onload = function () {
        media.nom = item.name;
        media.url = reader.result.split(',')[1];
        media.type = item.name.split('.')[item.name.split('.').length - 1];
        $this.medias.push(media);
      };
    }
    console.log(this.medias);
  }

  onUpload () {
      console.log('#############');
    this.submittingForm = true;
      this.mediaservice.createMedia(this.medias).subscribe(
        medias => {
          console.log('************************');
          console.log(medias);
          this.submittingForm = false;
          this.message = 'Chargement médias réussi!';
          setTimeout(
            () => this.message = undefined,
            2000
          );
          for (let m of medias) {
            const mediaArticle = new MediaArticle();
            mediaArticle.pk = new MediaArticlePk();
            mediaArticle.pk.media = m;
            this.a.mediaArticles.push(mediaArticle);
          }
          this.mediaOkk = true;
        },
        error => {
          if (error.status === 500) {
            this.message = 'ERREUR SERVEUR';
          } else {
            this.message = 'ERREUR CLIENT';
          }
          this.messageErr = error.error.message;
          console.log(error.message);
          console.log(error);
        }
        );

  }

  onSubmit () {
    if (this.a.id === undefined) {
      this.submittingForm = true;
      console.log(this.a.mediaArticles);
      for(let v of this.value){
        const articleTag = new ArticleTag();
        articleTag.pk = new ArticleTagPk();
        articleTag.pk.tag = v;
        this.a.articleTags.push(articleTag);
      }
      this.a.state = false;
      this.a.pageCible = 'eco';
      this.articleService.createArticle(this.a).subscribe(
        article => {
          this.back.emit(article);
          this.a = article;
          console.log('@@@@PARTENAIRE@@@');
          console.log(this.a.partenaire);
          this.submittingForm = false;
          this.message = 'Créé avec succès!';
          this.mediaOkk = false;
          // this.router.navigate(['pages/ecosystemes']);
          setTimeout(
            () => this.message = undefined,
            2000
          );
          this.router.navigate(['pages/ecosystemes/show-article/' + this.a.id]);
        },
        error => {
          if (error.status === 500) {
            this.message = 'ERREUR SERVEUR';
          } else {
            this.message = 'ERREUR CLIENT';
          }
          this.messageErr = error.error.message;
          console.log(error.message);
          console.log(error);
        }
      );
    } else {
      this.submittingForm = true;
      this.articleService.editArticle(this.a).subscribe(
        article => {
          console.log(this.a.mediaArticles);
          console.log('*********************************');
          this.back.emit(article);
          this.submittingForm = false;
          this.message = 'Mise à jour réussie!';
          setTimeout(
            () => this.message = undefined,
            2000
          );
          this.router.navigate(['pages/ecosystemes/show-article/' + this.a.id ]);
        },
        error => {
          if (error.status === 500) {
            this.message = 'ERREUR SERVEUR';
          } else {
            this.message = 'ERREUR CLIENT';
          }
          this.messageErr = error.error.message;
          console.log(error.message);
          console.log(error);
        }
      );

    }
  }


  public value: any = [''];
  public _disabledV: string = '0';
  public disabled: boolean = false;

  public selected (value: any): void {
    console.log('Selected value is: ', value);
  }

  public removed (value: any): void {
    console.log('Removed value is: ', value);
  }

  public refreshValue (value: any): void {
    this.value = value;
  }

  public itemsToString (value: Array<any> = []): string {
    return value
      .map((item: any) => {
        return item.text;
      }).join(',');
  }
  ngOnChanges (changes: SimpleChanges): void {
    if (this.a && this.a.corps) {
      this.defaultContentAr = this.a.corps;
    }
  }
  onSupp(media: Media, a: Article){
    this.articleService.deleteMedia(media, a).subscribe(
      () => {
        for (let i = 0; i < a.mediaArticles.length; i++) {
          if (a.mediaArticles[i].pk.media.id === media.id) {
            a.mediaArticles.splice(i, 1);
            break;
          }
        }
        this.articleService.editArticle(a).subscribe(
          a => { a = a;},
          error => {
            if (error.status === 500) {
              this.message = 'ERREUR SERVEUR';
            } else {
              this.message = 'ERREUR CLIENT';
            }
            this.messageErr = error.error.message;
            console.log(error.message);
            console.log(error);
          }
        );
      },
      error => {
        this.messageErr = error.error.message;
        console.log(error.message);
        console.log(error);
      }
    );
  }
}
