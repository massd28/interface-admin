import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Article} from '../../models/article';
import {Partenaire} from '../../models/partenaire';
import {ArticleService} from '../../services/article.service';
import {PartenaireService} from '../../services/partenaire.service';


@Component({
  selector: 'app-ecosystemes',
  templateUrl: './ecosystemes.component.html',
  styleUrls: ['./ecosystemes.component.scss'],
  providers: [ArticleService, PartenaireService]
})
export class EcosystemesComponent implements OnInit {
  articles: Article[];
  parts: Partenaire[];
  notif: string;
  pageSize = 10;
  pageNumber = 1;
  term: string;
  term1: string;
  popped: boolean;
  gettingData: boolean;
  deleting: boolean;
  constructor(private router: Router,
              private articleService: ArticleService,
              private partService: PartenaireService) { }

  ngOnInit() {
    this.term1 = this.term = '';
    console.log('After getArticles');
    this.getArticles();
    console.log('After getParts');
    this.getParts();
    this.deleting = false;
  }
  goToAddPartenaire(){
    this.router.navigate(['pages/ecosystemes/editor-part']);
  }

  goToAddArticleEcosystemes () {
    this.router.navigate(['pages/ecosystemes/editor-article']);
  }

 getArticles () {
    this.gettingData = true;
    this.articleService.getArticlesByPage('eco').subscribe(
      articles => {
        this.articles = articles;
        console.log(this.articles);
        this.gettingData = false;
      },
      error => {
        console.log(error);
      }
    );
  }

   getParts () {
    this.gettingData = true;
    this.partService.getPartenaires().subscribe(
      parts => {
        this.parts = parts;
        console.log(this.parts);
        this.gettingData = false;
      },
      error => {
        console.log(error);
      }
    );
  }
  pageChanged(pN: number): void {
    this.pageNumber = pN;
  }

  onSuppArt (article: Article) {
    this.deleting = true;
    this.articleService.deleteArticle(article).subscribe(
      () => {
        for (let i = 0; i < this.articles.length; i++) {
          if (this.articles[i].id === article.id) {
            this.articles.splice(i, 1);
            break;
          }
        }
        this.deleting = false;
        this.notif = 'Supprimée avec succès!';
        setTimeout(
          () => this.notif = undefined,
          2000
        );
      },
      error1 => {console.log(error1);}
    );
    this.router.navigate(['pages/ecosystemes']);
  }

  onSuppPart (part: Partenaire) {
    this.deleting = true;
    this.partService.deletePartenaire(part).subscribe(
      () => {
        for (let i = 0; i < this.parts.length; i++) {
          if (this.parts[i].id === part.id) {
            this.parts.splice(i, 1);
            break;
          }
        }
        this.deleting = false;
        this.notif = 'Supprimé avec succès!';
        setTimeout(
          () => this.notif = undefined,
          2000
        );
      },
      error1 => {console.log(error1);}
    );
    this.router.navigate(['pages/ecosystemes']);
  }

  onVoirPart (part: Partenaire) {
    this.router.navigate(['pages/ecosystemes/show-partenaire/', part.id]);
  }

  onVoirArt (article: Article) {
    this.router.navigate(['pages/ecosystemes/show-article/', article.id]);

  }

  getState(a: Article){
    if (a.state === true){
      return "Publié";
    }
    if (a.state === false) {
      return "Brouillon";
    }
  }
}
