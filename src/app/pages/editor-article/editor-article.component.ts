import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Article} from '../../models/article';
import {ArticleService} from '../../services/article.service';
import { FileUploader } from 'ng2-file-upload';
import {Media} from '../../models/media';
import {MediaService} from '../../services/media.service';
import {MediaArticle} from '../../models/media-article';
import {MediaArticlePk} from '../../models/media-article-pk';
import {Router} from '@angular/router';
import {TagService} from '../../services/tag.service';
import {Tag} from '../../models/tag';
import {ArticleTag} from '../../models/article-tag';
import {ArticleTagPk} from '../../models/article-tag-pk';

const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
  selector: 'app-editor-article',
  templateUrl: './editor-article.component.html',
  styleUrls: ['./editor-article.component.scss'],
  providers: [ArticleService, MediaService, TagService]
})
export class EditorArticleComponent implements OnInit {

  public uploader: FileUploader = new FileUploader({url: URL});
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;
  titre: string;

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }


  formArticle: FormGroup;
  @Output() back: EventEmitter<Article> = new EventEmitter();
  @Input() a: Article;
  m: Media;
  mediaOkk: boolean = false;
  media: Media;
  message: string;
  articles: Article[];
  medias: Media[];
  article: Article;
  submitting: boolean;
  @Input() pellContent;
  casCreation: boolean;
  public items: Array<any>;

  feedBack(ar: Article): void {
    this.articles.push(ar);
  }
  tags: Tag[];
  ajoutTag: boolean = false;
  formTag: FormGroup;
  @Output() backTag: EventEmitter<Tag> = new EventEmitter();
  t: Tag;



  ngOnInit(): void {
    this.article = new Article();

    this.article.corps = '';
    this.articles = this.articleService.articles;
    this.getArticles();
    console.log('After getArticles()');
    this.getTags();


  }

  onSubmitTag() {
    this.submitting = true;
    if (this.t.id === undefined) {
      this.tagService.createTag(this.t).subscribe(
        tag => {
          this.backTag.emit(tag);

          this.ajoutTag = false;
          const item = {
            id: tag.id,
            text: tag.nom
          };
          this.value.push(item);
          this.submitting = false;
        },
        error => {
          console.log(error);
        }
      );

    } else {
      this.tagService.editTag(this.t).subscribe(
        tag => {
          this.backTag.emit(tag);
          this.submitting = false;
        },
        error => {
          console.log(error);
        }
      );

    }
  }

  private getTags() {
    this.tagService.getTags().subscribe(
      tags => {
        this.tags = tags;
        for (let t of this.tags){
          const item = {
            id: t.id,
            text: t.nom
          }
          this.items.push(item) ;
        }
        console.log(this.items);
      },
      error => {
        console.log(error);

      }
    );
  }

  onContentChange(event: string) {
    this.a.corps = event;
    console.log(this.a.corps);
  }

  constructor(private fB: FormBuilder,
              private router: Router,
              private articleService: ArticleService,
              private mediaservice: MediaService,
              private tagService: TagService) {
    this.formArticle = this.fB.group({
      'titre': ['', Validators.required],

    });
    this.formTag = this.fB.group({
      'nom': ['', Validators.required],

    });
    this.t = new Tag();
    this.value = [];
    if (this.a === undefined){
      this.casCreation = true;
      this.titre = 'Nouvel article';
      this.a = new Article();
      this.a.corps = '';
      this.a.mediaArticles = [];
      this.a.articleTags = [];
    } else{
      this.titre = 'Modification';
      this.casCreation = false;
    }

    this.medias = [];
    this.m = new Media();
    this.items = [];
  }

  ajouterTag(){
    this.ajoutTag = true;
  }

  public fileDropped(event: any) {

    const $this = this;
    for(let item of event){
      const media = new Media;
      let reader = new FileReader();
      console.log(item);
     reader.readAsDataURL(item);
      reader.onload = function () {
      media.nom = item.name;
      //recupérer base 64 dans media.url
      media.url = reader.result.split(',')[1];
      media.type = item.name.split('.')[item.name.split('.').length - 1];
      $this.medias.push(media);
      };
    }
    console.log(this.medias);
  }
  onUpload() {
    console.log('#############');
    this.submitting = true;
      this.mediaservice.createMedia(this.medias).subscribe(
        medias => {
          console.log('************************');
          console.log(medias);
          this.submitting = false;
          this.message = 'Médias chargés avec succès!';
          for(let m of medias){
            const mediaArticle = new MediaArticle();
            mediaArticle.pk = new MediaArticlePk();
            mediaArticle.pk.media = m;
            this.a.mediaArticles.push(mediaArticle);
          }
          this.mediaOkk = true;
  });
 }


   onSubmit() {
    this.submitting = true;
  if (this.a.id === undefined) {
    console.log(this.a.mediaArticles);
    for(let v of this.value){
      const articleTag = new ArticleTag();
      articleTag.pk = new ArticleTagPk();
      articleTag.pk.tag = v;
      this.a.articleTags.push(articleTag);
    }
    this.mediaOkk = true;
      this.a.state = false;
      this.a.news = true;
      this.articleService.createArticle(this.a).subscribe(
        article => {
          this.back.emit(article);
          this.submitting = false;
          this.message = 'Article créé avec succès!';
          setTimeout(
            () => this.message = undefined,
            2000
          );
          this.router.navigate(['pages/articles/showarticle/' + article.id]);
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.articleService.editArticle(this.a).subscribe(
        article => {
          this.back.emit(article);
          this.submitting = false;
          this.message = 'Mise à jour réussie!';
          setTimeout(
            () => this.message = undefined,
            2000
          );
          this.router.navigate(['pages/articles']);
        },
        error => {
          console.log(error);
        }
      );

    }
  }

  private getArticles() {
    this.articleService.getArticles().subscribe(
      articles => {
        this.articles = articles;
        console.log(this.articles);
        for (let a of this.articles){
          this.items.push(a.titre);
        }
      },
      error => {
        console.log(error);
      }
    );
  }




  public value: any = [''];
  public _disabledV: string = '0';
  public disabled: boolean = false;

  public selected(value: any): void {
    console.log('Selected value is: ', value);
  }

  public removed(value: any): void {
    console.log('Removed value is: ', value);
  }

  public refreshValue(value: any): void {
    this.value = value;
  }

  public itemsToString(value: Array<any> = []): string {
    return value
      .map((item: any) => {
        return item.text;
      }).join(',');
  }
  onSupp(media: Media, a: Article){
    this.articleService.deleteMedia(media, a).subscribe(
      () => {
        for (let i = 0; i < a.mediaArticles.length; i++) {
          if (a.mediaArticles[i].pk.media.id === media.id) {
            a.mediaArticles.splice(i, 1);
            break;
          }
        }
        this.articleService.editArticle(a).subscribe(
          a => { a = a;},
          error => {
            if (error.status === 500) {
              this.message = 'ERREUR SERVEUR';
            } else {
              this.message = 'ERREUR CLIENT';
            }
            console.log(error);
          }
        );
      },
      error => {
        console.log(error);
      }
    );
  }
}

