import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './editor-article.routing';
import { SharedModule } from '../../shared/shared.module';
import {ModalModule} from 'ngx-modal';
import {NgxPaginationModule} from 'ngx-pagination';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EditorArticleComponent} from './editor-article.component';
import {FileDropDirective, FileUploader, FileUploadModule} from 'ng2-file-upload';
import {FileUploadComponent} from '../form/components/file-upload/file-upload.component';
import {SelectModule} from 'ng2-select';
import {EventsModule} from '../events/events.module';




@NgModule({
  declarations: [


    EditorArticleComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    FileUploadModule,
    ModalModule,
    routing,
    FileUploadModule,
    FormsModule,
    ReactiveFormsModule,
    SelectModule,

  ]
})
export class EditorArticleModule { }
