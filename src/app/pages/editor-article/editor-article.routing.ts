import { Routes, RouterModule } from '@angular/router';
import {EditorArticleComponent} from './editor-article.component';

const childRoutes: Routes = [
  {
    path: '',
    component: EditorArticleComponent,

  },


];

export const routing = RouterModule.forChild(childRoutes);
