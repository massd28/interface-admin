import {Component, EventEmitter, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ArticleService} from '../../../services/article.service';
import {MediaService} from '../../../services/media.service';
import {Article} from '../../../models/article';
import {TagService} from '../../../services/tag.service';
import {Tag} from '../../../models/tag';

@Component({
  selector: 'app-multiple-select',
  templateUrl: './multiple-select.component.html',
  providers: [TagService]
})
export class MultipleSelectComponent {
  public items: Array<any>;
  tags: Tag[];
  ajoutTag: boolean = false;
  formTag: FormGroup;
  @Output() back: EventEmitter<Tag> = new EventEmitter();
  t: Tag;
  submittingTag: boolean;

  feedBack(tag: Tag): void {
    this.tags.push(tag);
  }

  public value: any = this.items;
  public _disabledV: string = '0';
  public disabled: boolean = false;

  public selected(value: any): void {
    console.log('Selected value is: ', value);
  }

  public removed(value: any): void {
    console.log('Removed value is: ', value);
  }

  public refreshValue(value: any): void {
    this.value = value;
  }

  public itemsToString(value: Array<any> = []): string {
    return value
      .map((item: any) => {
        return item.text;
      }).join(',');
  }


  constructor(private fB: FormBuilder, private router: Router,
              private tagService: TagService) {  this.items = new Array<string>();
    this.formTag = this.fB.group({
      'nom': ['', Validators.required],

    });
    this.t = new Tag();
  this.value = [];
  }

  ngOnInit() {
    this.getTags();

  }
  ajouterTag(){
    this.ajoutTag = true;
  }

  onSubmit() {
    this.submittingTag = true;
    if (this.t.id === undefined) {
      this.tagService.createTag(this.t).subscribe(
        tag => {
          this.back.emit(tag);

          this.ajoutTag = false;
          const item = {
            id: tag.id,
            text: tag.nom
          }
          this.value.push(item);
          this.submittingTag = false;
        },
        error => {
          console.log(error);
        }
      );

    } else {
      this.tagService.editTag(this.t).subscribe(
        tag => {
          this.back.emit(tag);
          this.submittingTag = false;
        },
        error => {
          console.log(error);
        }
      );

    }
  }

  private getTags() {
    this.tagService.getTags().subscribe(
      tags => {
        this.tags = tags;
        for (let t of this.tags){
          const item = {
            id: t.id,
            text: t.nom
          }
          this.items.push(item) ;
        }
        console.log(this.items);
      },
      error => {
        console.log(error);

      }
    );
  }
}
