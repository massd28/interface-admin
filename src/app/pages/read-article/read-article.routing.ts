import { Routes, RouterModule } from '@angular/router';
import { ReadArticleComponent } from './read-article.component';


const childRoutes: Routes = [
  {
    path: 'read-article',
    component: ReadArticleComponent,
    children: [


    ]
  }
];

export const routing = RouterModule.forChild(childRoutes);
