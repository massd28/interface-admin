import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReadArticleComponent} from './read-article.component';
import { routing } from './read-article.routing';
import { SharedModule } from '../../shared/shared.module';
import {ModalModule} from 'ngx-modal';


@NgModule({
  declarations: [

  ],
  imports: [
    CommonModule,
    SharedModule,
    ModalModule,
    routing
  ]
})
export class ReadArticleModule { }
