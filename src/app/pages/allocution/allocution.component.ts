import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AppelDOffres} from '../../models/appel-d-offres';
import {FormBuilder} from '@angular/forms';
import {AppelDOffresService} from '../../services/appel-d-offres.service';
import {Router} from '@angular/router';
import {AllocutionService} from '../../services/allocution.service';
import {Allocution} from '../../models/allocution';
import {NgxSmartModalService} from 'ngx-smart-modal';

@Component({
  selector: 'app-allocution',
  templateUrl: './allocution.component.html',
  styleUrls: ['./allocution.component.scss'],
  providers: [AllocutionService, NgxSmartModalService]
})

  export class AllocutionComponent implements OnInit {
  @Output()back: EventEmitter<Allocution> = new EventEmitter();
  a: Allocution;
  message: string;
  allocutions: Allocution[];
  allocution: Allocution;
  term: String;

  /* pagination Info */
  pageSize = 10;
  pageNumber = 1;
  deleting: boolean;
  gettingAllocs: boolean;

  feedBack(al: Allocution): void {
    this.allocutions.push(al);
  }
  ngOnInit(): void {
    this.allocution = new Allocution();
    this.allocutions = this.allocutionService.allocutions;
    this.getAllocutions();
  }
  /*onDeleteEvents(appelDOffre: AppelDOffres): void {
    this.appelDOffreService.deleteAppelDOffres(appelDOffre).subscribe(
      () => {
        for (let i = 0; i < this.appelDOffres.length; i++) {
          if (this.appelDOffres[i].id === appelDOffre.id) {
            this.appelDOffres.splice(i, 1);
            break;
          }
        }
      }
    );
  }*/
  constructor(private fB: FormBuilder,
              private allocutionService: AllocutionService,
              public ngxSmartModalService: NgxSmartModalService,
              private router: Router) {

    this.a = new Allocution();

  }


  private getAllocutions() {
    this.gettingAllocs = true;
    this.allocutionService.getAllocutions().subscribe(
      allocutions => {
        this.allocutions = allocutions;
        this.gettingAllocs = false;
      },
      error => {
        console.log(error);
      }
    );
  }
  private goToEditor(){
    this.router.navigate(['pages/allocution/editor-allocution']);
  }

  onDeleteAlloc (a: Allocution) {
    /*this.deleting = true;
    this.allocutionService..subscribe(
      () => {
        for (let i = 0; i < this.appelDOffres.length; i++) {
          if (this.appelDOffres[i].id === appelDOffre.id) {
            this.appelDOffres.splice(i, 1);
            break;
          }
        }
        this.deleting = false;
      }, error => {
        console.log(error);
      }
    );*/
  }
}
