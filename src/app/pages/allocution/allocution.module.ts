import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import {ModalModule} from 'ngx-modal';
import {NgxPaginationModule} from 'ngx-pagination';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {routing} from './allocution.routing';
import {RouterModule} from '@angular/router';
import { EditorAllocutionComponent } from './editor-allocution/editor-allocution.component';
import {NgxSmartModalModule} from 'ngx-smart-modal';



@NgModule({
  declarations: [



  ],
  imports: [
    CommonModule,
    SharedModule,
    ModalModule,
    routing,
    RouterModule,
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSmartModalModule
  ]
})
export class AllocutionModule { }
