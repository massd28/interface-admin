import { Routes, RouterModule } from '@angular/router';
import {EditorEventsComponent} from '../events/editor-events/editor-events.component';
import {EditorAppelDoffresComponent} from '../editor/components/editor-appel-doffres/editor-appel-doffres.component';
import {AllocutionComponent} from './allocution.component';
import {EditorAllocutionComponent} from './editor-allocution/editor-allocution.component';



const childRoutes: Routes = [
  {
    path: '',
    component: AllocutionComponent,

  },

  {
    path: 'editor-allocution',
    component: EditorAllocutionComponent,

  },


];

export const routing = RouterModule.forChild(childRoutes);
