import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorAllocutionComponent } from './editor-allocution.component';

describe('EditorAllocutionComponent', () => {
  let component: EditorAllocutionComponent;
  let fixture: ComponentFixture<EditorAllocutionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorAllocutionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorAllocutionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
