import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AllocutionService} from '../../../services/allocution.service';
import { FileUploader } from 'ng2-file-upload';
import {Media} from '../../../models/media';
import {MediaService} from '../../../services/media.service';
import {Allocution} from '../../../models/allocution';
import {Router} from '@angular/router';

const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
  selector: 'app-editor-allocution',
  templateUrl: './editor-allocution.component.html',
  styleUrls: ['./editor-allocution.component.scss'],
  providers: [AllocutionService, MediaService]
})
export class EditorAllocutionComponent implements OnInit {

  public uploader: FileUploader = new FileUploader({url: URL});
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }


  formAllocution: FormGroup;
  @Output() back: EventEmitter<Allocution> = new EventEmitter();
  a: Allocution;
  m: Media;
  media: Media;
  message: string;
  allocutions: Allocution[];
  medias: Media[];
  allocution: Allocution;
  submitting: boolean;
  feedBack(al: Allocution): void {
    this.allocutions.push(al);
  }

  ngOnInit(): void {
    this.allocution = new Allocution();

    this.allocution.corps = '';
    this.allocutions = this.allocutionService.allocutions;
    //this.getAllocutions();
    console.log('After getArticles()');


  }

  onContentChange(event: string) {
    this.a.corps = event;
    console.log(this.a.corps);
  }

  constructor(private fB: FormBuilder,
              private router: Router,
              private allocutionService: AllocutionService,
              private mediaservice: MediaService) {
    this.formAllocution = this.fB.group({
      'titre': ['', Validators.required],

    });
    if(this.a === undefined) {
      this.a = new Allocution();
      this.a.corps = '';
    }


    this.medias = [];
    this.m = new Media();


  }


  public fileDropped(event: any) {

    const $this = this;
    for(let item of event){
      const media = new Media;
      let reader = new FileReader();
      console.log(item);
      reader.readAsDataURL(item);
      reader.onload = function () {
        media.nom = item.name;
        media.url = reader.result.split(',')[1];
        media.type = item.name.split('.')[item.name.split('.').length - 1];
        $this.medias.push(media);
      };
    }
    console.log(this.medias);
  }
  onUpload() {
    this.submitting = true;
    console.log("#############");
    this.mediaservice.createMedia(this.medias).subscribe(
      medias => {
        console.log("************************")
        console.log(medias);
        this.a.media = medias[0];
        this.submitting = false;
        this.message = 'Media(s) chargé(s) avec succès!';
        setTimeout(
          () => this.message = undefined,
          2000
        );
      });


  }


  onSubmit() {
    this.submitting = true;
    if (this.a.id === undefined) {
      this.allocutionService.saveAllocution(this.a).subscribe(
        allocution => {
          this.back.emit(allocution);
          this.submitting = false;
          this.message = 'Création réussie!';
          setTimeout(
            () => this.message = undefined,
            2000
          );
          this.router.navigate(['pages/allocution']);

        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.allocutionService.editAllocution(this.a).subscribe(
        allocution => {
          this.back.emit(allocution);
          this.submitting = false;
          this.message = 'Mise à jour réussie!';
          setTimeout(
            () => this.message = undefined,
            2000
          );
          this.router.navigate(['pages/allocution']);
        },
        error => {
          console.log(error);
        }
      );

    }
  }

  public items: Array<string>;



  public value: any = [''];
  public _disabledV: string = '0';
  public disabled: boolean = false;

  public selected(value: any): void {
    console.log('Selected value is: ', value);
  }

  public removed(value: any): void {
    console.log('Removed value is: ', value);
  }

  public refreshValue(value: any): void {
    this.value = value;
  }

  public itemsToString(value: Array<any> = []): string {
    return value
      .map((item: any) => {
        return item.text;
      }).join(',');
  }
}

