import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile.component';
import {AuthGuard} from '../../services/guard';

const childRoutes: Routes = [
    {
        path: '',
        component: ProfileComponent,
    }
];

export const routing = RouterModule.forChild(childRoutes);
