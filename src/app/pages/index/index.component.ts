import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { ChartsService } from '../charts/components/echarts/charts.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Message} from '../../models/message';
import {MessageService} from '../../services/message.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers: [MessageService]
})
export class IndexComponent implements OnInit {
  formEvent: FormGroup;
  @Output()back: EventEmitter<Message> = new EventEmitter();
  m: Message;
  messages: Message[];
  message: Message;
  feedBack(mes: Message): void {
    this.messages.push(mes);
  }
  ngOnInit(): void {
    this.message = new Message();
    this.messages = this.messageService.messages;
    this.getMessages();

  }

  private getMessages() {
    this.messageService.getMessages().subscribe(
      messages => {
        this.messages = messages;
        console.log(this.messages);
      },
      error => {
        console.log(error);
      }
    );
  }


  constructor(private fB: FormBuilder,
              private messageService: MessageService,
              private router: Router
  ) {

  }


}
