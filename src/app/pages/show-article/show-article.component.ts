import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Article} from '../../models/article';
import {ArticleService} from '../../services/article.service';
import {Observable} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import swal from "sweetalert2";
@Component({
  selector: 'app-show-article',
  templateUrl: './show-article.component.html',
  styleUrls: ['./show-article.component.css'],
  providers: [ArticleService]
})
export class ShowArticleComponent implements OnInit {
  @Output() back: EventEmitter<Article> = new EventEmitter();
  article: Article;
  publierAfter: boolean = false;
  idd: number;
  fr = {
    firstDayOfWeek: 0,
    dayNames: ['Dimanche', 'Lundi', 'Mardi','Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
    dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
    monthNames: [ 'Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Decembre' ],
    monthNamesShort: [ 'Jan', 'Fév', 'Mars', 'Avr', 'Mai', 'Juin','Juil', 'Août', 'Sep', 'Oct', 'Nov', 'Dec' ]
  };
  htmlStr: string = '<strong>The Tortoise</strong> &amp; the Hare';
  public mod = false;
  constructor(private articleService: ArticleService,
              private router: Router,
              private route: ActivatedRoute) {}
  ngOnInit(): void {
    this.idd = this.route.snapshot.params['id'];
    this.getArticles();

  }


  onPublier(a: Article) {
    a.state = true;
    a.datePublication = new Date();
    this.articleService.editArticle(a).subscribe(
      article => {
        this.back.emit(article);

        this.router.navigate(['pages/articles']);
        console.log(a.state);

      }
    );
  }

  getArticles(): void {
    this.articleService.getArticleById(this.route.snapshot.params['id']).subscribe(
      a => {
        a.images = [];
        a.videos = [];
        a.documents = [];
        this.articleService.getMediasByArticle(a.id).subscribe(
          medias => {
            for (let i = 0; i < medias.length; i++) {
              if (medias[i].type === 'png' || medias[i].type === 'jpg' || medias[i].type === 'jpeg') {
                a.images.push(medias[i]);
              }
              if (medias[i].type === 'mp4' || medias[i].type === 'mov' || medias[i].type === 'wav') {
                a.videos.push(medias[i]);
              }
              if (medias[i].type === 'pdf' || medias[i].type === 'txt') {
                a.documents.push(medias[i]);
              }
            }
          }
        );
        this.article = a;
        document.getElementById('texte').innerHTML = a.corps;

      }
    );
  }


  onPublerAfter(){
    this.publierAfter = true;
  }
  onValider(a: Article){
    a.state = true;
    a.datePublication = this.article.datePublication;
    this.articleService.editArticle(a).subscribe(
      article => {
        this.back.emit(article);

        this.router.navigate(['pages/articles']);
        console.log(a.state);

      }
    );
  }

  onMMM() {
    this.mod=true;
  }
}
