import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgxPaginationModule} from 'ngx-pagination';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../shared/shared.module';
import {RecrutementComponent} from './recrutement/recrutement.component';
import {AppelsCandidatureComponent} from './appels-candidature/appels-candidature.component';
import {routing} from './recrutement.routing';
import {EditorSessionComponent} from './appels-candidature/editor-session/editor-session.component';
import {NgxSmartModalModule, NgxSmartModalService} from 'ngx-smart-modal';
import {Ng2SearchPipeModule} from 'ng2-search-filter';
import {DateTimePickerModule} from 'ng-pick-datetime';
import {PosteComponent} from './poste/poste.component';
import {SelectModule} from 'ng2-select';
import {AppelDetailComponent} from './appels-candidature/appel-detail/appel-detail.component';
import {DossierCandidatComponent} from './dossier-candidat/dossier-candidat.component';
import {AddCommentComponent} from './dossier-candidat/add-comment/add-comment.component';
import {MultiSelectModule} from 'primeng/primeng';
import {PosteDetailComponent} from './appels-candidature/appel-detail/poste-detail/poste-detail.component';
import {SessionsEnCoursComponent} from './sessions-en-cours/sessions-en-cours.component';


@NgModule({
  imports: [
    NgxPaginationModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    routing,
    NgxSmartModalModule,
    Ng2SearchPipeModule,
    DateTimePickerModule,
    SelectModule,
    MultiSelectModule,

  ],
  declarations: [
    RecrutementComponent,
    AppelsCandidatureComponent,
    EditorSessionComponent,
    PosteComponent,
    AppelDetailComponent,
    DossierCandidatComponent,
    AddCommentComponent,
    PosteDetailComponent,
    SessionsEnCoursComponent
  ],
  providers: [NgxSmartModalService]
})
export class RecrutementModule { }
