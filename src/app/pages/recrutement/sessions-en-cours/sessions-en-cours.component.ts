import {Component, OnInit} from '@angular/core';
import {SessionRecrutement} from '../../../models/models-recrutement/session-recrutement';
import {SessionRecrutementService} from '../../../services/recrutement-services/session-recrutement.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sessions-en-cours',
  templateUrl: './sessions-en-cours.component.html',
  styleUrls: ['./sessions-en-cours.component.scss'],
  providers: [SessionRecrutementService]
})
export class SessionsEnCoursComponent implements OnInit {
  sessionsEnCours: SessionRecrutement[];

  constructor(private sessionRecrutementService: SessionRecrutementService,
              private router: Router) { }

  ngOnInit() {
    this.sessionsEnCours = [];
    this.sessionRecrutementService.getSessionEnCours().subscribe(
      sessions => {
       // console.log(sessions);
        this.sessionsEnCours = sessions;
      },
      error1 => {console.log(error1);}
    )
  }

  onVoirSessionEC(s: SessionRecrutement) {
    this.router.navigate(['pages/recrutement/appel-detail/'+ s.id]);
  }
}
