import {Component, OnInit} from '@angular/core';
import {SessionRecrutement} from '../../../../models/models-recrutement/session-recrutement';
import {SessionRecrutementService} from '../../../../services/recrutement-services/session-recrutement.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DossierCandidature} from '../../../../models/models-recrutement/dossier-candidature';
import {Poste} from '../../../../models/models-recrutement/poste';
import {SessionPoste} from '../../../../models/models-recrutement/session-poste';
import {SessionPostePk} from '../../../../models/models-recrutement/session-poste-pk';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PosteService} from '../../../../services/recrutement-services/poste.service';
import {DocumentAFournir} from '../../../../models/models-recrutement/document-a-fournir';
import {SelectItem} from 'primeng/primeng';

class PosteTemp {
  poste: Poste;
  docs: DocumentAFournir[];
}
@Component({
  selector: 'app-appel-detail',
  templateUrl: './appel-detail.component.html',
  styleUrls: ['./appel-detail.component.scss'],
  providers: [SessionRecrutementService, PosteService]
})
export class AppelDetailComponent implements OnInit {
  session: SessionRecrutement;
  message: string;
  deleting: boolean;
  p: Poste;
  formPoste: FormGroup;
  submittingPoste: boolean;
  postesSelected: PosteTemp[];
  posteTemps: SelectItem[];
  formSelectPoste: FormGroup;
  constructor(private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private sessionRecrutementService: SessionRecrutementService,
              private router: Router,
              private posteService: PosteService) { }

  ngOnInit() {
    this.session = new SessionRecrutement();
    this.session.sessionPostes = [];
    this.sessionRecrutementService.getSessionRecrutementById(this.route.snapshot.params['id']).subscribe(
      session => {
        this.session = session;
        this.sessionRecrutementService.getPostesBySession(this.session).subscribe(
          postes => {
            this.session.sessionPostes = postes;
          },
          err => {
            console.log(err);
          }
        )
      },
      error => {
        console.log(error);
      }
    );
    this.formSelectPoste = this.formBuilder.group({
      'postes': ['',]
    });
    this.posteTemps = [];
    this.postesSelected = [];
    this.posteService.getPostes().subscribe(
      postes => {
        for (let i=0; i < postes.length; i++) {
          this.posteTemps[i] = new class implements SelectItem {
            label: string;
            styleClass: string;
            value: any;
          };
          this.posteTemps[i].value = {
            poste: postes[i],
            docs: []
          };
          this.posteTemps[i].label = postes[i].nom;
        }
      },
      error1 => {
        this.message = error1.message;
      }
    );

    this.p = new Poste();
    this.formPoste = this.formBuilder.group({
      'poste': ['', Validators.required]
    });
  }
  onValiderPoste() {
    this.submittingPoste = true;
    this.posteService.createPoste(this.p).subscribe(
      p => {
        let sp = new SessionPoste();
        sp.pk = new SessionPostePk();
        sp.pk.poste = p;
        sp.pk.sessionRecrutement = this.session;
        this.sessionRecrutementService.createSessionPoste(sp).subscribe(
          sessionPoste => {
            sp = sessionPoste;
            this.session.sessionPostes.push(sp);
            this.sessionRecrutementService.editSessionRecrutement(this.session).subscribe(
              sess => {
                this.session = sess;
              },
              error1 => {console.log(error1);}
            )
          },
          err => {console.log(err);}
        );
        /**this.session.sessionPostes.push(sp);
        this.sessionRecrutementService.editSessionRecrutement(this.session).subscribe(
          session => {
            this.session = session;
          },
          error1 => {
            this.message = error1;
          }
        );*/
        this.submittingPoste = false;
        this.message = 'Ajout poste réussi';
        setTimeout(
           5000,
          this.message = undefined
        );
      },
      error1 => {
        this.message = error1.error.message;
      }
    );
    this.p.nom = '';
  }

  onVoirDossier(dossierCandidature: DossierCandidature) {
    this.router.navigate(['pages/recrutement/dossier-candidat/'+dossierCandidature.id]);
  }

  onDeleteSessionPoste(sp: SessionPoste) {
    console.log(sp);
    console.log('*******************');
    console.log(this.session.sessionPostes);
    console.log('@@@@@@@@@@@@@@@@@@@@');
    console.log(this.session);
    this.deleting = true;
    this.sessionRecrutementService.deleteSessionPoste(this.session,sp.pk.poste).subscribe(
      ()=> {
        for (let i = 0; i < this.session.sessionPostes.length; i++) {
          if (this.session.sessionPostes[i] === sp) {
            this.session.sessionPostes.splice(i, 1);
            break;
          }
        }
        this.deleting = false;
        this.message = 'Suppression OK!';
      },
      error1 => {
        this.deleting = false;
        this.message = error1.error.message;
      }
    )
  }

  onPostesOK() {

    if (this.postesSelected !== null || this.postesSelected !== undefined) {
      for (let p of this.postesSelected) {
        const sessionPoste = new SessionPoste();
        sessionPoste.documentAFournirs = p.docs;
        sessionPoste.pk = new SessionPostePk();
        sessionPoste.pk.poste = p.poste;
        sessionPoste.pk.sessionRecrutement = this.session;
        /**
        if (this.session.sessionPostes === null || this.session.sessionPostes === undefined){
          this.session.sessionPostes = [];
        }
         */
        this.sessionRecrutementService.createSessionPoste(sessionPoste).subscribe(
          sp => {
            this.session.sessionPostes.push(sp);
            this.sessionRecrutementService.editSessionRecrutement(this.session).subscribe(
              s => {this.session = s;},
              e =>{console.log(e);}
            );
          },
          e => {console.log(e);}
        );

      }
    }
  }
}
