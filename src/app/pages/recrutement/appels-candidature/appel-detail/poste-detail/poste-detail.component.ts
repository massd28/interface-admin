import {Component, Input, OnInit} from '@angular/core';
import {SessionPoste} from '../../../../../models/models-recrutement/session-poste';
import {DocumentAFournir} from '../../../../../models/models-recrutement/document-a-fournir';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SessionRecrutementService} from '../../../../../services/recrutement-services/session-recrutement.service';

@Component({
  selector: 'app-poste-detail',
  templateUrl: './poste-detail.component.html',
  styleUrls: ['./poste-detail.component.scss'],
  providers: [SessionRecrutementService]
})
export class PosteDetailComponent implements OnInit {
  @Input() sp: SessionPoste;
  addDocument: boolean;
  formDocument: FormGroup;
  nomDoc: string;
  descriptionDoc: string;
  constructor(private fb: FormBuilder,
              private sRService: SessionRecrutementService) { }

  ngOnInit() {
    this.formDocument = this.fb.group({
      'docs': this.fb.array([this.addFormDoc()])
    });
    console.log(this.sp);
    console.log(this.sp.pk);
  }
  addFormDoc (): FormGroup {
    return this.fb.group({
      'nom': ['', Validators.required],
      'description': ['', Validators.required],
    });
  }
  addDocButtonClick(i: number) {
    this.addDocument = true;
    const items = this.formDocument.get('docs') as FormArray;
    items.push(this.addFormDoc());
    items.removeAt(i);
  }

  onSuppDoc(doc: DocumentAFournir) {
    /** BREAK THE LINK doc AND sp FROM BACKEND*/
    this.sRService.deleteDoc(doc).subscribe(
      () => {
        for (let i = 0; i < this.sp.documentAFournirs.length; i++) {
          if (this.sp.documentAFournirs[i] === doc) {
            this.sp.documentAFournirs.splice(i, 1);
            break;
          }
        }
      },
      error1 => {console.log(error1);}
    );
    /** THEN UPDATE sp */
    this.sRService.editSessionPoste(this.sp).subscribe(
      sp => {
        this.sp = sp;
      },
      error1 => {
        console.log(error1);
      }
    );
  }

  onValiderDoc(nomDoc: string, descriptionDoc: string) {
    const doc = new DocumentAFournir();
    doc.nom = nomDoc;
    doc.description = descriptionDoc;
    if (this.sp.documentAFournirs === null || this.sp.documentAFournirs === undefined){
      this.sp.documentAFournirs = [];
    }
    this.sp.documentAFournirs.push(doc);
    /** THEN UPDATE sp */
    this.sRService.editSessionPoste(this.sp).subscribe(
      sp => {
        this.sp = sp;
      },
      error1 => {
        console.log(error1);
      }
    );
    /**const p = this.sp.pk.poste;
    console.log(p);
    this.sRService.getSessionRecrutementByPoste(p).subscribe(
      s => {
        console.log('##################');
        console.log(s);
        this.sRService.editSessionRecrutement(s).subscribe(
          sess => {
            this.sp.pk.sessionRecrutement = sess;
          },
          e => {console.log(e);}
        );
      },
      error1 => {console.log(error1);}
    );
*/
    this.addDocument = false;
    this.nomDoc = '';
    this.descriptionDoc = '';
  }
}
