import {Component, OnInit} from '@angular/core';
import {SessionRecrutement} from '../../../models/models-recrutement/session-recrutement';
import {SessionRecrutementService} from '../../../services/recrutement-services/session-recrutement.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-appels-candidature',
  templateUrl: './appels-candidature.component.html',
  styleUrls: ['./appels-candidature.component.scss'],
  providers: [SessionRecrutementService]
})
export class AppelsCandidatureComponent implements OnInit {
  sessionRecrutements: SessionRecrutement[];
  sessionEnCours: SessionRecrutement[];
  term = '';
  /* pagination Info */
  pageSize = 10;
  pageNumber = 1;
  /* END pagination Info */
  deleting: boolean;
  gettingSessions: boolean;
  message: string;
  constructor(private sessionRecrutementService: SessionRecrutementService,
              private router: Router) { }

  ngOnInit() {
    this.sessionRecrutements = [];
    this.gettingSessions = true;
    this.sessionRecrutementService.getSessionRecrutements().subscribe(
      sessions => {
        this.sessionRecrutements = sessions;
        console.log(this.sessionRecrutements);
        this.gettingSessions = false;
      },
      error1 => {
        this.message = error1.message;
      }
    );
    this.sessionRecrutementService.getSessionEnCours().subscribe(
      sessions => {
        this.sessionEnCours = sessions;
        console.log(this.sessionEnCours);
        this.gettingSessions = false;
      },
      error1 => {
        this.message = error1.message;
      }
    );
  }

  goToEditorSession() {
    this.router.navigate(['pages/recrutement/editor-session']);
  }
  onVoirSession(s: SessionRecrutement){
    this.router.navigate(['pages/recrutement/appel-detail/' + s.id]);

  }

  onDeleteSession(s: SessionRecrutement) {
    this.deleting = true;
    this.sessionRecrutementService.deleteSessionRecrutement(s).subscribe(
      () => {
        for (let i = 0; i < this.sessionRecrutements.length; i++) {
          if (this.sessionRecrutements[i].id === s.id) {
            this.sessionRecrutements.splice(i, 1);
            break;
          }
        }
        this.deleting = false;
        this.message = 'Supprimée avec succès!';
        setTimeout(
          () => this.message = undefined,
          2000
        );
      },
      error1 => {
        this.deleting = false;
        this.message = error1.message;
      }
    )
  }

  updateSession(s: SessionRecrutement) {
    this.router.navigate(['pages/recrutement/editor-session/'+ s.id]);
  }
}
