import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SessionRecrutementService} from '../../../../services/recrutement-services/session-recrutement.service';
import {SessionRecrutement} from '../../../../models/models-recrutement/session-recrutement';
import {PosteService} from '../../../../services/recrutement-services/poste.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SessionPoste} from '../../../../models/models-recrutement/session-poste';
import {SelectItem} from 'primeng/primeng';
import {DocumentAFournir} from '../../../../models/models-recrutement/document-a-fournir';
import {NgxSmartModalService} from 'ngx-smart-modal';

@Component({
  selector: 'app-editor-session',
  templateUrl: './editor-session.component.html',
  styleUrls: ['./editor-session.component.scss'],
  providers: [SessionRecrutementService, PosteService]
})
export class EditorSessionComponent implements OnInit {
  @Input()vvggb: any;
  formSession: FormGroup;
  s: SessionRecrutement;
  fr = {
    firstDayOfWeek: 0,
    dayNames: ['Dimanche', 'Lundi', 'Mardi','Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
    dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
    monthNames: [ 'Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Decembre' ],
    monthNamesShort: [ 'Jan', 'Fév', 'Mars', 'Avr', 'Mai', 'Juin','Juil', 'Août', 'Sep', 'Oct', 'Nov', 'Dec' ]
  };
  @Output() backSession: EventEmitter<SessionRecrutement> = new EventEmitter();
  submittingSession: boolean;
  message: string;
  //postes: SelectItem[];
  addDoc: boolean;
  //formDoc: FormGroup;
  doc: DocumentAFournir;
  //docOk: boolean;
  docs: DocumentAFournir[];
  //showDocsForm: boolean;

  items: SelectItem[];

  item: string;

  /**
   * nomDoc = '';
   descriptionDoc = '';
   */
  sessionPostes: SessionPoste[];

  constructor(private formBuilder: FormBuilder,
              private sessionRecrutementService: SessionRecrutementService,
              private posteService: PosteService,
              private router: Router,
              public ngxSmartModalService: NgxSmartModalService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.s = new SessionRecrutement();
    if (this.route.snapshot.params['id']){
      this.sessionRecrutementService.getSessionRecrutementById(this.route.snapshot.params['id']).subscribe(
        session => {this.s = session;},
        e => {console.log(e);}
      );
    }
    /**

    */
    this.formSession = this.formBuilder.group({
      'titre': ['', Validators.required],
      'description': ['', Validators.required],
      //'postes': [''],
      'dateDebut': ['', Validators.required],
      'dateFin': ['', Validators.required],
      //'docs': this.formBuilder.array([this.addFormDoc()])
    });

  }
  /**
  addFormDoc (): FormGroup{
    return this.formBuilder.group({
      'nom': [''],
      'description': [''],
    });
  }
   */
  // convenience getter for easy access to form fields
  get f() { return this.formSession.controls; }
  /**
  onValidDoc(posteTemp: PosteTemp, nomDoc: string, descriptionDoc: string) {
    const doc = new DocumentAFournir();
    doc.nom = nomDoc;
    doc.description = descriptionDoc;
    posteTemp.docs.push(doc);
    this.nomDoc = this.descriptionDoc = '';
    this.docOk = true;
    this.addDoc = false;
    console.log(posteTemp);
  }
   */
  onSubmit() {
    /**
     *
    this.s.sessionPostes = this.sessionPostes;
    console.log(this.s.sessionPostes);
    //console.log(this.postesSelected);
 */
    this.submittingSession = true;
    if (this.s.id === undefined){
      this.sessionRecrutementService.createSessionRecrutement(this.s).subscribe(
        session => {
          this.backSession.emit(session);
          this.s = session;
          this.submittingSession = false;
          this.router.navigate(['pages/recrutement/appel-detail/'+this.s.id]);
        },
        error1 => {
          this.message = error1.message;
        }
      );
    }else {
      this.sessionRecrutementService.editSessionRecrutement(this.s).subscribe(
        session => {
          this.backSession.emit(session);
          this.s = session;
          this.submittingSession = false;
          this.router.navigate(['pages/recrutement/appel-detail/'+this.s.id]);        },
        error1 => {
          this.message = error1.message;
        }
      );
    }
  }
 /**
  addDocButtonClick(i: number) {
    this.addDoc = true;
    const items = this.formSession.get('docs') as FormArray;
    items.push(this.addFormDoc());
    items.removeAt(i);

  }
*
  onSuppDocList(doc: DocumentAFournir, p: PosteTemp) {
    for (let i = 0; i < p.docs.length; i++) {
      if (p.docs[i] === doc) {
        p.docs.splice(i, 1);
        break;
      }
    }
  }
  */
}
