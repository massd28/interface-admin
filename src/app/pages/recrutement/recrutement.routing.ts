import {RouterModule, Routes} from '@angular/router';
import {RecrutementComponent} from './recrutement/recrutement.component';
import {AppelsCandidatureComponent} from './appels-candidature/appels-candidature.component';
import {EditorSessionComponent} from './appels-candidature/editor-session/editor-session.component';
import {PosteComponent} from './poste/poste.component';
import {AppelDetailComponent} from './appels-candidature/appel-detail/appel-detail.component';
import {DossierCandidatComponent} from './dossier-candidat/dossier-candidat.component';
import {AddCommentComponent} from './dossier-candidat/add-comment/add-comment.component';

const childRoutes: Routes = [
  {
    path: '',
    component: RecrutementComponent,
  },
  {path: '', redirectTo: 'appels', pathMatch: 'full'},
  {path:'appels', component: AppelsCandidatureComponent},
  {path: 'editor-session', component: EditorSessionComponent},
  {path: 'editor-session/:id', component: EditorSessionComponent},
  {path: 'poste', component: PosteComponent},
  {path: 'appel-detail/:id', component: AppelDetailComponent},
  {path: 'dossier-candidat/:id', component: DossierCandidatComponent},
  {path: 'add-comment/:id', component: AddCommentComponent},
];

export const routing = RouterModule.forChild(childRoutes);
