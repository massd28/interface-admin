import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Poste} from '../../../models/models-recrutement/poste';
import {PosteService} from '../../../services/recrutement-services/poste.service';
import {Router} from '@angular/router';
import {NgxSmartModalService} from 'ngx-smart-modal';


@Component({
  selector: 'app-poste',
  templateUrl: './poste.component.html',
  styleUrls: ['./poste.component.scss'],
  providers: [PosteService]
})
export class PosteComponent implements OnInit {
  poste = false;
  formPoste: FormGroup;
  p: Poste;
  term = '';
  @Output() backPoste: EventEmitter<Poste> = new EventEmitter();
  submittingPoste: boolean;
  message: string;
  postes: Poste[];
  gettingPostes: boolean;
  /* pagination Info */
  pageSize = 10;
  pageNumber = 1;
  /* END pagination Info */
  items: FormArray;
  deleting: boolean;


  constructor(private posteService: PosteService,
              private formbuilder: FormBuilder,
              private router: Router,
              public modalService: NgxSmartModalService) { }

  ngOnInit() {
    this.postes = [];
    this.gettingPostes = true;
    this.getPostes();
    this.p = new Poste();
    this.modalService = new NgxSmartModalService();
    this.formPoste = this.formbuilder.group({
      'poste': ['', Validators.required],
    });
  }

  onValiderPoste() {
    this.submittingPoste = true;
    /**let sp = new SessionPoste();
    sp.documentAFournirs = this.docs;
    sp.pk = new SessionPostePk();
    sp.pk.poste = this.p;
     */
    if (this.p.id === undefined) {
      this.posteService.createPoste(this.p).subscribe(
        poste => {
          this.backPoste.emit(poste);
          this.submittingPoste = false;
          this.message = 'Poste ajouté avec succès';
          setTimeout(
            () => this.message = undefined,
            2000
          );
        },
        error1 => {
          this.message = error1.message;
          setTimeout(
            () => this.message = undefined,
            10000
          );
        }
      );
    } else {
      this.posteService.editPoste(this.p).subscribe(
        poste => {
          this.backPoste.emit(poste);
          this.submittingPoste = false;
          this.message = 'Poste ajouté avec succès';
          setTimeout(
            () => this.message = undefined,
            2000
          );
          //this.modalService.getModal('posteModal').close();
        },
        error1 => {
          this.message = error1.message;
          setTimeout(
            () => this.message = undefined,
            10000
          );
        }
      );
    }
    this.poste = false;
    this.postes.push(this.p);
    this.p = new Poste();
  }
  pageChanged(pN: number): void {
    this.pageNumber = pN;
  }
  getPostes() {
    this.posteService.getPostes().subscribe(
      postes => {
        this.postes = postes;
        this.gettingPostes = false;
      },
      error1 => {
        this.message = error1.message;
      }
    );
  }

  onSuppPoste(p: Poste) {
    this.deleting = true;
    this.posteService.deletePoste(p).subscribe(
      () => {
        for (let i = 0; i < this.postes.length; i++) {
          if (this.postes[i].id === p.id) {
            this.postes.splice(i, 1);
            break;
          }
        }
        this.deleting = false;
        this.message = 'Suppression OK';
        setTimeout(
          () => this.message = undefined,
          2000
        );
        //this.modalService.getModal('suppModal').close();
      },
      err => {
        this.deleting = false
        this.message = err.error.message;
        setTimeout(
          () => this.message = undefined,
          5000
        );
        //this.modalService.getModal('suppModal').close();
      }
    )
  }
}
