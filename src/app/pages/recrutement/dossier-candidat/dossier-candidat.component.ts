import {Component, OnInit} from '@angular/core';
import {DossierCandidature} from '../../../models/models-recrutement/dossier-candidature';
import {DossierCandidatureService} from '../../../services/recrutement-services/dossier-candidature.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Commentaire} from '../../../models/models-recrutement/commentaire';

@Component({
  selector: 'app-dossier-candidat',
  templateUrl: './dossier-candidat.component.html',
  styleUrls: ['./dossier-candidat.component.scss'],
  providers: [DossierCandidatureService]
})
export class DossierCandidatComponent implements OnInit {
  dossier: DossierCandidature;
  constructor(private dossierCandidatureService: DossierCandidatureService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.dossier = new DossierCandidature();
    this.dossier.commentaires = [];
    this.dossier.diplomes = [];
    this.dossier.experiencePros = [];
    this.dossierCandidatureService.getDossierCandidatureById(this.route.snapshot.params['id']).subscribe(
      d => {
        this.dossier = d;
      },
      error => {
        console.log(error);
      }
    );
    this.dossier = {
      id: 1,
      etat: 'gneugneu',
      candidat: {
        id: 1,
        prenom: 'fgfghdgh',
        nom: 'geghfdh',
        email: 'dhjdfjhjd',
        username: 'kjfgthri',
        password: 'hejzjz',
        dossierCandidatures:[this.dossier]
      },
      experiencePros: [
        {
          id: 1,
          description: 'dfezaf',
          dateDebut: new Date(),
          dateFin: new Date(),
          dossierCandidature: this.dossier,
          entreprise: 'azerty',
          posteOccupe:'DG'
        },
        {
          id: 2,
          description: 'dfezaf',
          dateDebut: new Date(),
          dateFin: new Date(),
          dossierCandidature: this.dossier,
          entreprise: 'azerty',
          posteOccupe:'DG'
        },
        {
          id: 3,
          description: 'dfezafcvd',
          dateDebut: new Date(),
          dateFin: new Date(),
          dossierCandidature: this.dossier,
          entreprise: 'azertyazet',
          posteOccupe:'DGbffdhjk'
        }
      ],
      diplomes: [
        {
          id: 1,
          structure: 'ept',
          anneeObtention: '2018',
          nom: 'DIC',
          dossierCandidature: this.dossier
        },
        {
          id: 2,
          structure: 'isept',
          anneeObtention: '2014',
          nom: 'TC',
          dossierCandidature: this.dossier
        },
      ],
      commentaires: [
        {
          id: 1,
          corps:'sfjgfg eghejkzgheyrv',
          dossierCandidature: this.dossier
        },
        {
          id: 2,
          corps:'sfjgfSJHSDERYG EGERYg eghejkzgheyrv',
          dossierCandidature: this.dossier
        },
        {
          id: 3,
          corps:'QQSSEJ HGEZF HRTZEUI',
          dossierCandidature: this.dossier
        },
      ]
    }
  }

  onAjouterCom(dossier: DossierCandidature) {
    this.router.navigate(['pages/recrutement/add-comment/' + dossier.id]);
  }

  onEntretien(dossier: DossierCandidature) {

  }

  onSuppDossier(dossier: DossierCandidature) {
    this.dossierCandidatureService.deleteDossierCandidature(dossier).subscribe(
      () => {console.log('OK');},
      error => {console.log(error);}
    );
  }

  onSuppCom(c: Commentaire) {

  }
}
