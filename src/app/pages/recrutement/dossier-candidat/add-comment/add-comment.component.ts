import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {DossierCandidatureService} from '../../../../services/recrutement-services/dossier-candidature.service';
import {CommentaireService} from '../../../../services/recrutement-services/commentaire.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DossierCandidature} from '../../../../models/models-recrutement/dossier-candidature';
import {Commentaire} from '../../../../models/models-recrutement/commentaire';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-add-comment',
  templateUrl: './add-comment.component.html',
  styleUrls: ['./add-comment.component.scss'],
  providers: [DossierCandidatureService, CommentaireService]
})
export class AddCommentComponent implements OnInit {
  dossier: DossierCandidature;
  commentaire: Commentaire;
  formCom: FormGroup;
  @Output() backCom: EventEmitter<Commentaire> = new EventEmitter();
  adding: boolean;

  constructor(private dossierCandidatureService: DossierCandidatureService,
              private commentaireService: CommentaireService,
              private route: ActivatedRoute,
              private router: Router,
              private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.dossier = new DossierCandidature();
    this.dossier.commentaires = [];
    this.dossierCandidatureService.getDossierCandidatureById(this.route.snapshot.params['id']).subscribe(
      dossier => {
        this.dossier = dossier;
      },
      error => {
        console.log(error);
      }
    );
    this.commentaire = new Commentaire();
    this.formCom = this.formBuilder.group({
      'corps': ['', Validators.required],
    });
  }
  onValiderCom(){
    this.adding = true;
    console.log(this.commentaire);
    console.log(this.dossier);
    console.log(this.dossier.commentaires);
    this.dossier.commentaires.push(this.commentaire);
    this.commentaireService.createCommentaire(this.commentaire).subscribe(
      com => {
        this.backCom.emit(com);
        console.log('com is ok');
        this.adding = false
      },
      err => {
        console.log(err);
        this.adding = false;
      }
    );
  }
}
