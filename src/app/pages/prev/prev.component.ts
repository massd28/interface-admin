import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Article} from '../../models/article';
import {ArticleService} from '../../services/article.service';
import {Observable} from 'rxjs';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-prev',
  templateUrl: './prev.component.html',
  styleUrls: ['./prev.component.scss'],
  providers: [ArticleService]
})
export class PrevComponent implements OnInit {

  @Output() back: EventEmitter<Article> = new EventEmitter();
  article: Article;
  idd: number;
  htmlStr: string = '<strong>The Tortoise</strong> &amp; the Hare';
  constructor(private articleService: ArticleService,
              private route: ActivatedRoute) {}
  ngOnInit(): void {
    this.idd = this.route.snapshot.params['id'];
    this.getArticles();
  }




  getArticles(): void {
    this.articleService.getArticleById(this.route.snapshot.params['id']).subscribe(
      a => {
        a.images = [];
        a.videos = [];
        a.documents = [];
        this.articleService.getMediasByArticle(a.id).subscribe(
          medias => {
            for (let i = 0; i < medias.length; i++) {
              if (medias[i].type === 'png' || medias[i].type === 'jpg' || medias[i].type === 'jpeg') {
                a.images.push(medias[i]);
              }
              if (medias[i].type === 'mp4' || medias[i].type === 'mov' || medias[i].type === 'wav') {
                a.videos.push(medias[i]);
              }
              if (medias[i].type === 'pdf' || medias[i].type === 'txt') {
                a.documents.push(medias[i]);
              }
            }
          }
        );
        this.article = a;
        document.getElementById('texte').innerHTML = a.corps;

      }
    );
  }
}
