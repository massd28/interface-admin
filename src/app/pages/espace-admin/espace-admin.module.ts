import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EspaceAdminComponent } from './espace-admin.component';
import { routing } from './espace-admin.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxEchartsModule } from 'ngx-echarts';
import {ProfileComponent} from '../profile/profile.component';
import {ModalModule} from 'ngx-modal';
import {RouterModule} from '@angular/router';
import {NgxPaginationModule} from 'ngx-pagination';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SelectModule} from 'ng2-select';


@NgModule({
  declarations: [



  ],
  imports: [CommonModule,
    SharedModule,
    NgxEchartsModule,
    routing
  ]
})
export class EspaceAdminModule{}
