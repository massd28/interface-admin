import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Message} from '../../../models/message';
import {MessageService} from '../../../services/message.service';
import {FileUploader} from 'ng2-file-upload';


const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';
@Component({
  selector: 'app-messagerie',
  templateUrl: './editor-message.component.html',
  styleUrls: ['./editor-message.component.scss'],
  providers: [MessageService]
})
export class EditorMessageComponent implements OnInit {
  public uploader: FileUploader = new FileUploader({url: URL});
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  formMessage: FormGroup;
  @Output()back: EventEmitter<Message> = new EventEmitter();
  m: Message;
  messages: Message[];
  message: Message;
  feedBack(mes: Message): void {
    this.messages.push(mes);
  }
  ngOnInit(): void {
    this.message = new Message();
    this.messages = this.messageService.messages;
    this.getMessages();

  }

  private getMessages() {
    this.messageService.getMessages().subscribe(
      messages => {
        this.messages = messages;
        console.log(this.messages);
      },
      error => {
        console.log(error);
      }
    );
  }


  constructor(private fB: FormBuilder,
              private messageService: MessageService,
              private router: Router
  ) {
    this.formMessage = this.fB.group({
      'emailDestinataire': ['', Validators.required],
      'objet': ['', Validators.required],
      'corps': ['', Validators.required],

    });
    this.m = new Message();
    this.messages = [];

  }
  onContentChange(event: string) {
    this.m.corps = event;
    console.log(this.m.corps);
  }

  onSubmit() {
    if (this.m.id === undefined) {
      this.m.dateEnvoi = new Date();
      this.m.sent = true;
      this.m.emailEmetteur = "isepdd2018@gmail.com";
      this.messageService.createMessage(this.m).subscribe(
        message => {
          this.back.emit(message);
          this.router.navigate(['pages/espace-admin/messages-sent']);
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.messageService.editMessage(this.m).subscribe(
        message => {
          this.back.emit(message);
          this.router.navigate(['pages/espace-admin/messages-sent']);
        },
        error => {
          console.log(error);
        }
      );

    }
  }


}


