import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorMessageComponent } from './editor-message.component';

describe('EditorMessageComponent', () => {
  let component: EditorMessageComponent;
  let fixture: ComponentFixture<EditorMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
