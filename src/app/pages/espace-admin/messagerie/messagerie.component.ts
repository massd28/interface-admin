import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { ChartsService} from '../../charts/components/echarts/charts.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Message} from '../../../models/message';
import {MessageService} from '../../../services/message.service';

@Component({
  selector: 'app-messagerie',
  templateUrl: './messagerie.component.html',
  styleUrls: ['./messagerie.component.scss'],
  providers: [MessageService]
})
export class MessagerieComponent implements OnInit {
  /* pagination Info */
  pageSize = 10;
  pageNumber = 1;

  formMessage: FormGroup;
  @Output()back: EventEmitter<Message> = new EventEmitter();
  m: Message;
  messages: Message[];
  messagesSent: Message[];
  messagesReceived: Message[];
  messagesLu: Message[];
  messagesNonLu: Message[];
  message: Message;
  feedBack(mes: Message): void {
    this.messages.push(mes);
  }
  ngOnInit(): void {
    this.message = new Message();
    this.messages = this.messageService.messages;
    this.getMessages();
    this.getMessagesSent();
    this.getMessagesReceived();
    this.getMessagesLu();
    this.getMessagesNonLu();

  }

  private getMessages() {
    this.messageService.getMessages().subscribe(
      messages => {
        this.messages = messages;
        console.log(this.messages);
      },
      error => {
        console.log(error);
      }
    );
  }

  private getMessagesSent() {
    this.messageService.getMessagesSent().subscribe(
      messages => {
        this.messagesSent = messages;
        console.log(this.messagesSent);
      },
      error => {
        console.log(error);
      }
    );
  }

  private getMessagesReceived() {
    this.messageService.getMessagesReceived().subscribe(
      messages => {
        this.messagesReceived = messages;
        console.log(this.messagesReceived);
      },
      error => {
        console.log(error);
      }
    );
  }

  private getMessagesLu() {
    this.messageService.getMessagesLu().subscribe(
      messages => {
        this.messagesLu = messages;
        console.log(this.messagesLu);
      },
      error => {
        console.log(error);
      }
    );
  }

  private getMessagesNonLu() {
    this.messageService.getMessagesNonLu().subscribe(
      messages => {
        this.messagesNonLu = messages;
        console.log(this.messagesNonLu);
      },
      error => {
        console.log(error);
      }
    );
  }

  constructor(private fB: FormBuilder,
              private messageService: MessageService,
              private router: Router
  ) {
    this.messages = [];
    this.messagesSent = [];
    this.messagesReceived = [];
    this.messagesLu = [];
    this.messagesNonLu = [];
    this.formMessage = this.fB.group({
      'emailDestinataire': ['', Validators.required],
      'objet': ['', Validators.required],
      'corps': ['', Validators.required],
    });
    this.m = new Message();

  }
  newMessage(){
    this.router.navigate(['pages/espace-admin/editor-message']);
  }
  ouvrirMessage(mess: Message){
   mess.lu = true;
    console.log(mess.lu);
    this.messageService.editMessage(mess).subscribe(
      message => {
        this.back.emit(message);

        this.router.navigate(['pages/espace-admin/show-message/' + mess.id]);


      }
    );
  }

  pageChanged(pN: number): void {
    this.pageNumber = pN;
  }


  onSubmit() {
    if (this.m.id === undefined) {
      this.m.sent = true;
      this.m.emailEmetteur = "isepdd2018@gmail.com";
      this.messageService.createMessage(this.m).subscribe(
        message => {
          this.back.emit(message);
          this.router.navigate(['pages/espace-admin/messages-sent']);
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.messageService.editMessage(this.m).subscribe(
        message => {
          this.back.emit(message);
          this.router.navigate(['pages/espace-admin/messages-sent']);
        },
        error => {
          console.log(error);
        }
      );

    }
  }


}
