import { Routes, RouterModule } from '@angular/router';
import {MessagerieComponent} from './messagerie/messagerie.component';
import {EditorMessageComponent} from './editor-message/editor-message.component';
import {ReponseMessageComponent} from './reponse-message/reponse-message.component';
import {MessagesSentComponent} from './messages-sent/messages-sent.component';
import {ShowMessageComponent} from './show-message/show-message.component';




const childRoutes: Routes = [
  {
    path: 'messagerie',
    component: MessagerieComponent,

  },

  {
    path: 'editor-message',
    component: EditorMessageComponent,

  },
  {
    path: 'messages-sent',
    component: MessagesSentComponent,

  },
  {
    path: 'reponse-message',
    component: ReponseMessageComponent,

  },
  {
    path: 'show-message/:id',
    component: ShowMessageComponent,

  },


];

export const routing = RouterModule.forChild(childRoutes);
