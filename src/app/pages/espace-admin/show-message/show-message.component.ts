import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Message} from '../../../models/message';
import {MessageService} from '../../../services/message.service';
import {Observable} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import swal from "sweetalert2";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Label} from '../../../models/label';



@Component({
  selector: 'app-show-message',
  templateUrl: './show-message.component.html',
  styleUrls: ['./show-message.component.scss'],
  providers: [MessageService]
})
export class ShowMessageComponent implements OnInit {
  formMessage: FormGroup;
  @Output()back: EventEmitter<Message> = new EventEmitter();
  m: Message;
  message: Message;
  idd: number;
  repondre: boolean;
  deleting: boolean;

  constructor(private messageService: MessageService,
              private router: Router,
              private fB: FormBuilder,
              private route: ActivatedRoute,
              ) {
    this.formMessage = this.fB.group({
      'corps': ['', Validators.required],
    });
  }
  ngOnInit(): void {
    this.idd = this.route.snapshot.params['id'];
    this.getMessages();
    this.m = new Message;
    this.message = new Message;

  }

onRepondre(){
    this.repondre = true;
}



  getMessages(): void {
    this.messageService.getMessageById(this.route.snapshot.params['id']).subscribe(
      m => {
        this.message = m;

      }
    );
  }


  onSubmit() {
    if (this.m.id === undefined) {
      this.m.sent = true;
      this.m.objet = this.message.objet;
      this.m.emailEmetteur = "isepdd2018@gmail.com";
      this.m.emailDestinataire = this.message.emailEmetteur;
      this.messageService.createMessage(this.m).subscribe(
        message => {
          this.back.emit(message);
          this.router.navigate(['pages/espace-admin/messages-sent']);
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.messageService.editMessage(this.m).subscribe(
        message => {
          this.back.emit(message);
          this.router.navigate(['pages/espace-admin/messages-sent']);
        },
        error => {
          console.log(error);
        }
      );

    }
  }



  onDeleteMessage(message: Message): void {
    this.deleting = true;
    this.messageService.deleteMessage(message).subscribe(
      () => {
        this.deleting = false;
        this.router.navigate(['pages/espace-admin/messagerie']);
      }
    );
  }

}
