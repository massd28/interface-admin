import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReponseMessageComponent } from './reponse-message.component';

describe('ReponseMessageComponent', () => {
  let component: ReponseMessageComponent;
  let fixture: ComponentFixture<ReponseMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReponseMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReponseMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
