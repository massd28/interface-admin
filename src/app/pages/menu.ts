export let MENU_ITEM = [
 /* {
    path: 'form',
    title: 'Forms',
    icon: 'check-square-o',
    children: [
      {
        path:'form-inputs',
        title: 'Form Inputs'
      },
      {
        path: 'form-layouts',
        title: 'Form Layouts'
      },
      {
        path: 'file-upload',
        title: 'File Upload'
      },
      {
        path: 'ng2-select',
        title: 'Ng2-Select'
      }
    ]
  },*/
  {
        path: 'espace-admin',
        title: 'Espace Admin',
        icon: 'user',
        children: [
                   {
        path: 'messagerie',
        title: 'Messagerie',
        icon: 'envelope'
                       },

    ]

  },

  /*{
   path: 'profile',
   title: 'My Account',
   icon: 'user'
 },
  {
       path: 'editor',
       title: 'Pell Editor',
       icon: 'pencil'
   },
    {
        path: 'icon',
        title: 'Icon',
        icon: 'diamond'
    },

  */
  {
    path: 'articles',
    title: 'Gestion des articles',
    icon: 'file-text-o',



  },
  {
    path: 'events',
    title: 'Gestion des Evènements',
    icon: 'calendar',



  },
  {
    path: 'annonces',
    title: 'Gestion Appel Offre',
    icon: 'calendar',

  },

  {
    path: 'departements',
    title: 'Gestion départements',
    icon: 'home'
  },


    {
        path: 'ecosystemes',
        title: 'Gestion Ecosystèmes',
        icon: 'users'
    },
  {
    path: 'allocution',
    title: 'Gestion Allocutions',
    icon: 'users'
  },
  {
    path: 'labels',
    title: 'Gestion Labels',
    icon: 'file-text-o'
  },
  {
    path: 'recrutement',
    title: 'Gestion recrutement',
    icon: 'table',
    children: [
      {
        path: 'appels',
        title: 'Appels a candidatures',
        icon: 'book'
      },
      {
        path: 'poste',
        title: 'Postes',
        icon: 'check'
      },/*
      {
        path: 'candidatures',
        title: 'Candidatures',
        icon: 'file'
      },
      {
        path: 'entretiens',
        title: 'Entretiens',
        icon: 'user'
      },*/
    ]
  },
   /* {
        path: 'ui',
        title: 'UI Element',
        icon: 'paint-brush',
        children: [
            {
                path: 'grid',
                title: 'Bootstrap Grid'
            },
            {
                path: 'buttons',
                title: 'Buttons'
            },
            {
                path: 'notification',
                title: 'Notification'
            },
            {
                path: 'tabs',
                title: 'Tabs'
            },
            {
                path: 'file-tree',
                title: 'File Tree'
            },
            {
                path: 'modals',
                title: 'Modals'
            },
            {
                path: 'progress-bar',
                title: 'ProgressBar'
            },
              {
                 path: 'loading',
                 title: 'Loading'
             },
        ]
    },*/
   /*
    {
        path: 'form',
        title: 'Forms',
        icon: 'check-square-o',
        children: [
            {
                path: 'form-inputs',
                title: 'Form Inputs'
            },
            {
                path: 'form-layouts',
                title: 'Form Layouts'
            },
            {
                path: 'file-upload',
                title: 'File Upload'
            },
            {
                path: 'ng2-select',
                title: 'Ng2-Select'
            }
        ]
    },*/
    /*{
        path: 'charts',
        title: 'Charts',
        icon: 'bar-chart',
        children: [
            {
                path: 'echarts',
                title: 'Echarts'
            }
        ]
    },
    {
        path: 'table',
        title: 'Tables',
        icon: 'table',
        children: [
            {
                path: 'basic-tables',
                title: 'Basic Tables'
            },
            {
                path: 'data-table',
                title: 'Data Table'
            }
        ]
    },
    {
        path: 'menu-levels',
        title: 'Menu Levels',
        icon: 'sitemap',
        children: [
            {
                path: 'levels1',
                title: 'Menu Level1',
                children: [
                    {
                        path: 'levels1-1',
                        title: 'Menu Level1-1'
                    }
                ]
            },
            {
                path: 'levels2',
                title: 'Menu Level2'
            }
        ]
    },*/
];
