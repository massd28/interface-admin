import {RouterModule, Routes} from '@angular/router';
import {PagesComponent} from './pages.component';
import {LoginComponent} from './login/login.component';
import {LabelsComponent} from './labels/labels.component';
import {EditorLabelsComponent} from './labels/editor-labels/editor-labels.component';


export const childRoutes: Routes = [
    {
        path: 'login',
        component: LoginComponent,
    },


    {
        path: 'pages',
        component: PagesComponent,
        children: [
            { path: '', redirectTo: 'index', pathMatch: 'full' },
            { path: 'index', loadChildren: './index/index.module#IndexModule' },
            { path: 'editor', loadChildren: './editor/editor.module#EditorModule' },
            { path: 'ecosystemes', loadChildren: './ecosystemes/ecosystemes.module#EcosystemesModule' },
            { path: 'editor-article', loadChildren: './editor-article/editor-article.module#EditorArticleModule' },
            { path: 'read-article', loadChildren: './read-article/read-article.module#ReadArticleModule' },
            { path: 'add-user', loadChildren: './add-user/add-user.module#AddUserModule' },
            { path: 'articles', loadChildren: './articles/articles.module#ArticlesModule' },
            { path: 'annonces', loadChildren: './appel-doffres/appel-doffres.module#AppelDoffresModule' },
            { path: 'annonces', loadChildren: './appel-doffres/appel-doffres.module#AppelDoffresModule' },
            { path: 'events', loadChildren: './events/events.module#EventsModule' },
            //{ path: 'medias', loadChildren: './medias/medias.module#MediasModule' },
            { path: 'icon', loadChildren: './icon/icon.module#IconModule' },
            { path: 'profile', loadChildren: './profile/profile.module#ProfileModule' },
            { path: 'form', loadChildren: './form/form.module#FormModule' },
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'ui', loadChildren: './ui/ui.module#UIModule' },
            { path: 'table', loadChildren: './table/table.module#TableModule' },
            { path: 'menu-levels', loadChildren: './menu-levels/menu-levels.module#MenuLevelsModule' },
            {path: 'departements', loadChildren: './departements/departements.module#DepartementsModule'},
            {path: 'allocution', loadChildren: './allocution/allocution.module#AllocutionModule'},
            {path: 'espace-admin', loadChildren: './espace-admin/espace-admin.module#EspaceAdminModule'},
            {path: 'errors', loadChildren: './errors/errors.module#ErrorsModule'},
            {path: 'recrutement', loadChildren: './recrutement/recrutement.module#RecrutementModule'},
          {
            path: 'labels',
            component: LabelsComponent,
          },

          {
            path: 'editor-labels',
            component: EditorLabelsComponent,
          },
        ]
    }
];

export const routing = RouterModule.forChild(childRoutes);
