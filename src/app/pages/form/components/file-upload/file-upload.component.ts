import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import {Media} from '../../../../models/media';
import {MediaService} from '../../../../services/media.service';
import {FormBuilder, Validators} from '@angular/forms';
import {ArticleService} from '../../../../services/article.service';
import {Article} from '../../../../models/article';

const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
  providers: [MediaService]
})
export class FileUploadComponent implements OnInit {
  @Output() back: EventEmitter<Media> = new EventEmitter();
  urlSelected: any = 'http://localhost/medias/img1.jpeg';
  media: Media;
  message: string;
  m: Media;
  str: String;
  mediaEncode: MSBaseReader;

  constructor(private mediaservice: MediaService) {
    this.m = new Media();
  }


  ngOnInit() {
  }


  public uploader: FileUploader = new FileUploader({url: URL});
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }

  public fileDropped(event: any) {

    const $this = this;
    let reader = new FileReader();
    reader.readAsDataURL(event[0]);
    reader.onload = function () {
      $this.m.nom = (event[0]).name;
      console.log($this.m.nom);
      console.log($this.m.id);
      $this.m.url = reader.result.split(",")[1];
      $this.m.type = (event[0]).name.split(".")[1];
      console.log($this.m.type);
    };
  }



}
