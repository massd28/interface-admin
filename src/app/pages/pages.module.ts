import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './pages.routing';
import { LayoutModule } from '../shared/layout.module';
import { SharedModule } from '../shared/shared.module';
import { PagesComponent } from './pages.component';
import { LoginComponent } from './login/login.component';
import {AuthGuard} from '../services/guard';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ShoweventsComponent} from './showevents/showevents.component';
import {ShowappelComponent} from './showappel/showappel.component';









@NgModule({
    imports: [
        CommonModule,
        LayoutModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        routing,

    ],
    declarations: [

        LoginComponent,
      PagesComponent,



    ],


})
export class PagesModule { }
