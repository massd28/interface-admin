import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Article} from '../../models/article';
import {ArticleService} from '../../services/article.service';
import {Router} from '@angular/router';
import {AppelDOffresService} from '../../services/appel-d-offres.service';
import {AppelDOffres} from '../../models/appel-d-offres';
import {Media} from '../../models/media';
import {MediaService} from '../../services/media.service';

@Component({
  selector: 'app-appel-doffres',
  templateUrl: './appel-doffres.component.html',
  styleUrls: ['./appel-doffres.component.scss'],
  providers: [AppelDOffresService]
})
export class AppelDOffresComponent implements OnInit {
  @Output()back: EventEmitter<AppelDOffres> = new EventEmitter();
  ap: AppelDOffres;
  message: string;
  appelDOffres: AppelDOffres[];
  appelDOffre: AppelDOffres;
  term: String;
  gettingAppels: boolean;
  deleting: boolean;

  /* pagination Info */
  pageSize = 10;
  pageNumber = 1;
  feedBack(ap: AppelDOffres): void {
    this.appelDOffres.push(ap);
  }
  ngOnInit(): void {
    this.appelDOffre = new AppelDOffres();
    this.appelDOffres = this.appelDOffreService.appelDOffres;
    this.getAppelDOffres();
    this.term = '';
  }
  onDeleteAppel(appelDOffre: AppelDOffres): void {
    this.deleting = true;
    this.appelDOffreService.deleteAppelDOffres(appelDOffre).subscribe(
      () => {
        for (let i = 0; i < this.appelDOffres.length; i++) {
          if (this.appelDOffres[i].id === appelDOffre.id) {
            this.appelDOffres.splice(i, 1);
            break;
          }
        }
        this.deleting = false;
      }, error => {
        console.log(error);
      }
    );
  }
  constructor(private fB: FormBuilder,
              private appelDOffreService: AppelDOffresService,
              private router: Router) {

    this.ap = new AppelDOffres();

  }


  private getAppelDOffres() {
    this.gettingAppels = true;
    this.appelDOffreService.getAppelDOffres().subscribe(
      appelDOffres => {
        this.appelDOffres = appelDOffres;
        this.gettingAppels = false;
      },
      error => {
        console.log(error);
      }
    );
  }
  goToEditor(){
    this.router.navigate(['pages/annonces/editor-appel-doffres']);
  }
  getState(a: AppelDOffres){
    if (a.state === true){
      return "Publié";
    }
    if (a.state === false) {
      return "Brouillon";
    }
  }


  pageChanged(pN: number): void {
    this.pageNumber = pN;
  }

  onVoirAppel(a: AppelDOffres) {
   this.router.navigate(['pages/annonces/showappel/' + a.id]);
  }
}
