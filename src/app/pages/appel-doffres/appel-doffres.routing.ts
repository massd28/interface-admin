import { Routes, RouterModule } from '@angular/router';
import {AppelDOffresComponent} from '../appel-doffres/appel-doffres.component';
import {ShowArticleComponent} from '../show-article/show-article.component';
import {ShowappelComponent} from '../showappel/showappel.component';
import {EditorAppelDoffresComponent} from '../editor/components/editor-appel-doffres/editor-appel-doffres.component';



const childRoutes: Routes = [
  {
    path: '',
    component: AppelDOffresComponent,

  },
  {path:'showappel/:id', component: ShowappelComponent,},
  {
    path: 'editor-appel-doffres',
    component: EditorAppelDoffresComponent,

  },

];

export const routing = RouterModule.forChild(childRoutes);
