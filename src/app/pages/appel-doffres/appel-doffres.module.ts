import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ModalModule} from 'ngx-modal';
import {NgxPaginationModule} from 'ngx-pagination';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../shared/shared.module';
import {routing} from './appel-doffres.routing';
import {RouterModule} from '@angular/router';
import {NgxSmartModalModule} from 'ngx-smart-modal';
//import {EditorAppelDoffresComponent} from '../editor/components/editor-appel-doffres/editor-appel-doffres.component';
import {FileUploadModule} from 'ng2-file-upload';
import {SelectModule} from 'ng2-select';
import {ShowappelComponent} from '../showappel/showappel.component';
import {EditorAppelDoffresComponent} from '../editor/components/editor-appel-doffres/editor-appel-doffres.component';




@NgModule({
  declarations: [

  EditorAppelDoffresComponent,
    ShowappelComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ModalModule,
    routing,
    RouterModule,
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSmartModalModule,
    FileUploadModule,
    SelectModule

  ]
})
export class AppelDoffresModule { }
