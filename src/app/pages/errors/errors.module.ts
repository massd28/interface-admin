import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../../shared/shared.module';
import {ErrorComponent} from './error/error.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [ErrorComponent]
})
export class ErrorsModule { }
