import { Routes, RouterModule } from '@angular/router';
import {ErrorComponent} from './error/error.component';

const childRoutes: Routes = [
  {
    path: 'error/:message',
    component: ErrorComponent
  },
  {
    path: '',
    component: ErrorComponent
  },
];

export const routing = RouterModule.forChild(childRoutes);
