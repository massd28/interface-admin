import { Routes, RouterModule } from '@angular/router';
import { EditorComponent } from './editor.component';
import {EditorEventsComponent} from '../events/editor-events/editor-events.component';
import {EditorAppelDoffresComponent} from './components/editor-appel-doffres/editor-appel-doffres.component';



const childRoutes: Routes = [
    {
        path: 'editor-events',
        component: EditorEventsComponent,

    },

  {
    path: 'editor-appel-doffres',
    component: EditorAppelDoffresComponent,

  },



];

export const routing = RouterModule.forChild(childRoutes);
