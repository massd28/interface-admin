import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorAppelDOffresComponent } from './editor-appel-doffres.component';

describe('EditorAppelDOffresComponent', () => {
  let component: EditorAppelDOffresComponent;
  let fixture: ComponentFixture<EditorAppelDOffresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorAppelDOffresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorAppelDOffresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
