import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AppelDOffres} from '../../../../models/appel-d-offres';
import {AppelDOffresService} from '../../../../services/appel-d-offres.service';
import {Router} from '@angular/router';
import { FileUploader } from 'ng2-file-upload';
import {FileDropDirective} from 'ng2-file-upload';
import {Media} from '../../../../models/media';
import {MediaService} from '../../../../services/media.service';
import {Event} from '../../../../models/event';
import {EventService} from '../../../../services/event.service';
import {MediaArticle} from '../../../../models/media-article';
import {MediaArticlePk} from '../../../../models/media-article-pk';
import {ArticleService} from '../../../../services/article.service';
import {Article} from '../../../../models/article';

const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
  selector: 'app-editor-appel-doffres',
  templateUrl: './editor-appel-doffres.component.html',
  styleUrls: ['./editor-appel-doffres.component.scss'],
  providers: [AppelDOffresService, MediaService, ArticleService]
})
export class EditorAppelDoffresComponent implements OnInit {

  public uploader: FileUploader = new FileUploader({url: URL});
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }


  formAppel: FormGroup;
  @Output() back: EventEmitter<AppelDOffres> = new EventEmitter();
  @Input() a: AppelDOffres;
  m: Media;
  mediaOkk: boolean = false;
  media: Media;
  message: string;
  appelDOffres: AppelDOffres[];
  medias: Media[];
  appelDOffre: AppelDOffres;
  submitting: boolean;
  deleting: boolean;
  feedBack(ap: AppelDOffres): void {
    this.appelDOffres.push(ap);
  }

  ngOnInit(): void {
    this.appelDOffre = new AppelDOffres();

    this.appelDOffre.corps = '';
    this.appelDOffres = this.appelDOffresService.appelDOffres;
    this.getAppelDOffres();
    console.log('After getArticles()');


  }

  onContentChange(event: string) {
    this.a.corps = event;
    console.log(this.a.corps);
  }

  constructor(private fB: FormBuilder,
              private router: Router,
              private appelDOffresService: AppelDOffresService,
              private mediaservice: MediaService,
              private articleService: ArticleService) {
    this.formAppel = this.fB.group({
      'titre': ['', Validators.required],
      'corps': ['', Validators.required],

    });
    if(this.a === undefined) {
      this.a = new AppelDOffres();
      this.a.mediaArticles = [];
    }
    this.medias = [];
    this.m = new Media();
    this.items = new Array<string>();

  }


  public fileDropped(event: any) {

    const $this = this;
    for(let item of event){
      const media = new Media;
      let reader = new FileReader();
      console.log(item);
      reader.readAsDataURL(item);
      reader.onload = function () {
        media.nom = item.name;
        media.url = reader.result.split(',')[1];
        media.type = item.name.split('.')[item.name.split('.').length - 1];
        $this.medias.push(media);
      };
    }
    console.log(this.medias);
  }


  onUpload() {
    this.submitting = true;
    console.log('#############');
    this.mediaservice.createMedia(this.medias).subscribe(
      medias => {
        console.log('************************');
        console.log(medias);
        this.submitting = false;
        this.message = 'Médias chargés avec succès!';
        setTimeout(
          () => this.message = undefined,
          2000
        );
        for(let m of medias){
          const mediaArticle = new MediaArticle();
          mediaArticle.pk = new MediaArticlePk();
          mediaArticle.pk.media = m;
          this.a.mediaArticles.push(mediaArticle);
        }
        this.mediaOkk = true;
      });


  }


  onSubmit() {
    this.submitting = true;
    if (this.a.id === undefined) {
      console.log(this.a.mediaArticles);
      this.a.state = false;
      this.a.news = false;
      this.appelDOffresService.createAppelDOffres(this.a).subscribe(
        appelDOffre => {
          this.back.emit(appelDOffre);
          this.submitting = false;
          this.message = 'Appel lancé avec succès!';
          setTimeout(
            () => this.message = undefined,
            2000
          );
          this.mediaOkk = false;
          this.router.navigate(['pages/annonces']);
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.appelDOffresService.editAppelDOffres(this.a).subscribe(
        appelDOffre => {
          this.back.emit(appelDOffre);
          this.submitting = false;
          this.message = 'Mise à jour réussie!';
          setTimeout(
            () => this.message = undefined,
            2000
          );
          this.router.navigate(['pages/annonces']);
        },
        error => {
          console.log(error);
        }
      );

    }
  }

  private getAppelDOffres() {
    this.appelDOffresService.getAppelDOffres().subscribe(
      appelDOffres => {
        this.appelDOffres = appelDOffres;
        for (let a of this.appelDOffres){
          this.items.push(a.titre);
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  public items: Array<string>;



  public value: any = [''];
  public _disabledV: string = '0';
  public disabled: boolean = false;

  public selected(value: any): void {
    console.log('Selected value is: ', value);
  }

  public removed(value: any): void {
    console.log('Removed value is: ', value);
  }

  public refreshValue(value: any): void {
    this.value = value;
  }

  public itemsToString(value: Array<any> = []): string {
    return value
      .map((item: any) => {
        return item.text;
      }).join(',');
  }
  onSupp(media: Media, a: Article){
    this.deleting = true;
    this.articleService.deleteMedia(media, a).subscribe(
      () => {
        for (let i = 0; i < a.mediaArticles.length; i++) {
          if (a.mediaArticles[i].pk.media.id === media.id) {
            a.mediaArticles.splice(i, 1);
            break;
          }
        }
        this.articleService.editArticle(a).subscribe(
          a => { a = a;},
          error => {
            if (error.status === 500) {
              this.message = 'ERREUR SERVEUR';
            } else {
              this.message = 'ERREUR CLIENT';
            }
            console.log(error);
          }
        );
        this.deleting = false;
      },
      error => {
        console.log(error);
      }
    );
  }
}






