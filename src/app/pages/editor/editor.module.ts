import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import {ModalModule} from 'ngx-modal';
import {NgxPaginationModule} from 'ngx-pagination';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EditorComponent} from './editor.component';
import { EditorArticleComponent} from '../editor-article/editor-article.component';
import { EditorEventsComponent} from '../events/editor-events/editor-events.component';
import {EditorAppelDoffresComponent} from './components/editor-appel-doffres/editor-appel-doffres.component';
import {routing} from './editor.routing';
import {RouterModule} from '@angular/router';
import {SelectModule} from 'ng2-select';
import {DateTimePickerModule} from 'ng-pick-datetime';
import {FileUploadModule} from 'ng2-file-upload';




@NgModule({
  declarations: [
    //EditorAppelDoffresComponent,
   // EditorEventsComponent


  ],
  imports: [
    CommonModule,
    SharedModule,
    ModalModule,
    routing,
    RouterModule,
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    DateTimePickerModule,
    SelectModule,
    FileUploadModule
  ]
})
export class EditorModule { }
