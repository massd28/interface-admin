import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Article} from '../../models/article';
import {ArticleService} from '../../services/article.service';
import { FileUploader } from 'ng2-file-upload';
//import {Tag} from '../../models/tag';
import {TagService} from '../../services/tag.service';

const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss'],
  providers: [ArticleService]
})
export class EditorComponent implements OnInit {

  formArticle: FormGroup;
  @Output()back: EventEmitter<Article> = new EventEmitter();
  a: Article;
  message: string;
  articles: Article[];
  //tags: Tag[];
  article: Article;
  feedBack(ar: Article): void {
    this.articles.push(ar);
  }
  ngOnInit(): void {
    this.article = new Article();
    this.article.corps = '';
      //    this.tags = this.tagService.tags;
    //this.getTags();
    this.articles = this.articleService.articles;
    this.getArticles();
    console.log('After getArticles()');
    document.getElementById('text-output').innerHTML = this.a.corps;
  }

  public uploader: FileUploader = new FileUploader({ url: URL });
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }


  onContentChange(event: string) {
    this.a.corps = event;
    console.log(this.a.corps);
  }
  onDeleteArticle(article: Article): void {
    this.articleService.deleteArticle(article).subscribe(
      () => {
        for (let i = 0; i < this.articles.length; i++) {
          if (this.articles[i].id === article.id) {
            this.articles.splice(i, 1);
            break;
          }
        }
      }
    );
  }
  constructor(private fB: FormBuilder,
              private articleService: ArticleService,
              private tagService: TagService) {
    this.formArticle = this.fB.group({
      'titre': ['', Validators.required]
    });
    this.a = new Article();
  }

  onSubmit() {
    if (this.a.id === undefined) {
      this.articleService.createArticle(this.a).subscribe(
        article => {
          this.back.emit(article);
          this.message = 'Successfully created!';
          setTimeout(
            () => this.message = undefined,
            2000
          );
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.articleService.editArticle(this.a).subscribe(
        article => {
          this.back.emit(article);
          this.message = 'Successfully updated!';
          setTimeout(
            () => this.message = undefined,
            2000
          );
        },
        error => {
          console.log(error);
        }
      );
    }
  }
  private getArticles() {
    this.articleService.getArticles().subscribe(
      articles => {
        this.articles = articles;
        console.log(this.articles);
      },
      error => {
        console.log(error);
      }
    );
  }


  /*public items: Array<string> = ['Amsterdam', 'Antwerp', 'Athens', 'Barcelona',
    'Berlin', 'Birmingham', 'Bradford', 'Bremen', 'Brussels', 'Bucharest',
    'Budapest', 'Cologne', 'Copenhagen', 'Dortmund', 'Dresden', 'Dublin', 'Düsseldorf',
    'Essen', 'Frankfurt', 'Genoa', 'Glasgow', 'Gothenburg', 'Hamburg', 'Hannover',
    'Helsinki', 'Leeds', 'Leipzig', 'Lisbon', 'Łódź', 'London', 'Kraków', 'Madrid',
    'Málaga', 'Manchester', 'Marseille', 'Milan', 'Munich', 'Naples', 'Palermo',
    'Paris', 'Poznań', 'Prague', 'Riga', 'Rome', 'Rotterdam', 'Seville', 'Sheffield',
    'Sofia', 'Stockholm', 'Stuttgart', 'The Hague', 'Turin', 'Valencia', 'Vienna',
    'Vilnius', 'Warsaw', 'Wrocław', 'Zagreb', 'Zaragoza'];*/

  public items: Array<string> = [];

  //public value: any = [''+ this.tags[0].nom];
  public _disabledV: string = '0';
  public disabled: boolean = false;

  public selected(value: any): void {
    console.log('Selected value is: ', value);
  }

  public removed(value: any): void {
    console.log('Removed value is: ', value);
  }

  /*public refreshValue(value: any): void {
    this.value = value;
  }*/

  public itemsToString(value: Array<any> = []): string {
    return value
      .map((item: any) => {
        return item.text;
      }).join(',');
  }

  /*private getTags(): void {
    this.tagService.getTags().subscribe(
      tags => {
        this.tags = tags;
        console.log(this.tags);
      },
      error => {
        console.log(error);
      }
    );
  }*/
}

