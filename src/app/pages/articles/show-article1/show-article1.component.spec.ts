import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowArticle1Component } from './show-article1.component';

describe('ShowArticle1Component', () => {
  let component: ShowArticle1Component;
  let fixture: ComponentFixture<ShowArticle1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowArticle1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowArticle1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
