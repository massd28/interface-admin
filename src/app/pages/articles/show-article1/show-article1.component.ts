import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Article} from '../../../models/article';
import {ArticleService} from '../../../services/article.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-show-article1',
  templateUrl: './show-article1.component.html',
  styleUrls: ['./show-article1.component.scss'],
  providers: [ArticleService]
})
export class ShowArticle1Component implements OnInit {

  @Output() back: EventEmitter<Article> = new EventEmitter();
  article: Article;
  idd: number;
  htmlStr: string = '<strong>The Tortoise</strong> &amp; the Hare';
  modifAr = false;
  constructor(private articleService: ArticleService,
              private router: Router,
              private route: ActivatedRoute) {}
  ngOnInit(): void {
    this.idd = this.route.snapshot.params['id'];
    this.getArticles();

  }


  onPublier(a: Article) {
    a.state = !a.state;
    a.datePublication = new Date();
    this.articleService.editArticle(a).subscribe(
      article => {
        this.back.emit(article);

        this.router.navigate(['pages/articles']);
        console.log(a.state);

      }
    );
  }

  getArticles(): void {
    this.articleService.getArticleById(this.route.snapshot.params['id']).subscribe(
      a => {
        a.images = [];
        a.videos = [];
        a.documents = [];
        this.articleService.getMediasByArticle(a.id).subscribe(
          medias => {
            for (let i = 0; i < medias.length; i++) {
              if (medias[i].type === 'png' || medias[i].type === 'jpg' || medias[i].type === 'jpeg') {
                a.images.push(medias[i]);
              }
              if (medias[i].type === 'mp4' || medias[i].type === 'mov' || medias[i].type === 'wav') {
                a.videos.push(medias[i]);
              }
              if (medias[i].type === 'pdf' || medias[i].type === 'txt') {
                a.documents.push(medias[i]);
              }
            }
          }
        );
        this.article = a;
        document.getElementById('texte').innerHTML = a.corps;

      }
    );
  }

  onModifierArticle (article: Article) {
    this.articleService.getArticleById(this.route.snapshot.params['id']);
    this.modifAr = true;
    ///this.router.navigateByUrl('pages/editor-article');
  }

}
