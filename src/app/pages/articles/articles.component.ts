import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Article} from '../../models/article';
import {ArticleService} from '../../services/article.service';
import {Router} from '@angular/router';
import {NgxSmartModalService} from 'ngx-smart-modal';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss'],
  providers: [ArticleService, NgxSmartModalService]
})
export class ArticlesComponent implements OnInit {

  formArticle: FormGroup;
  @Output()back: EventEmitter<Article> = new EventEmitter();
  a: Article;
  message: string;
  articles: Article[];
  article: Article;
  term: String;
  deleting: boolean;
  gettingArticles: boolean;

  /* pagination Info */
  pageSize = 10;
  pageNumber = 1;

  feedBack(ar: Article): void {
    this.articles.push(ar);
  }
  ngOnInit(): void {
    this.article = new Article();
    this.articles = this.articleService.articles;
    this.getArticles();
    /*document.getElementById('text-output').innerHTML = this.a.corps;*/
    console.log('After getArticles()');
    this.term = '';
  }
  onDeleteArticle(article: Article): void {
    this.deleting = true;
    this.articleService.deleteArticle(article).subscribe(
      () => {
        for (let i = 0; i < this.articles.length; i++) {
          if (this.articles[i].id === article.id) {
            this.articles.splice(i, 1);
            break;
          }
        }
        this.deleting = false;
        this.router.navigate(['pages/articles']);
      }
    );
  }
  constructor(private fB: FormBuilder,
              private articleService: ArticleService,
              private router: Router
  ) {
    this.formArticle = this.fB.group({
      'titre': ['', Validators.required],
      'corps': ['', Validators.required]
    });
    this.a = new Article();
  }

  onSubmit() {
    if (this.a.id === undefined) {
      this.articleService.createArticle(this.a).subscribe(
        article => {
          this.back.emit(article);
          this.message = 'Successfully created!';
          setTimeout(
            () => this.message = undefined,
            2000
          );
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.articleService.editArticle(this.a).subscribe(
        article => {
          this.back.emit(article);
          this.message = 'Successfully updated!';
          setTimeout(
            () => this.message = undefined,
            2000
          );
        },
        error => {
          console.log(error);
        }
      );
    }
  }
  getArticles() {
    this.gettingArticles = true;
    this.articleService.getNews().subscribe(
      articles => {
        this.articles = articles;
        console.log(this.articles);
        this.gettingArticles = false;
      },
      error => {
        console.log(error);
      }
    );
  }

  goToEditor(){
    this.router.navigate(['pages/articles/editor-article']);
  }
  getState(a: Article){
    if (a.state === true){
      return "Publié";
    }
    if (a.state === false) {
      return "Brouillon";
    }
  }

  pageChanged(pN: number): void {
    this.pageNumber = pN;
  }

  onVoirArticle(a: Article) {
    this.router.navigate(['pages/articles/showarticle/' + a.id]);
  }
}
