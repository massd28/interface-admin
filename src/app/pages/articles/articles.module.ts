import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ModalModule} from 'ngx-modal';
import {NgxPaginationModule} from 'ngx-pagination';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../shared/shared.module';
import {routing} from './articles.routing';
import {RouterModule} from '@angular/router';
import {NgxSmartModalModule} from 'ngx-smart-modal';


@NgModule({
  declarations: [


  ],
  imports: [
    CommonModule,
    SharedModule,
    ModalModule,
    routing,
    RouterModule,
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSmartModalModule

  ]
})
export class ArticlesModule { }

