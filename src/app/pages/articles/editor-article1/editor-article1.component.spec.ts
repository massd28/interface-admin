import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorArticle1Component } from './editor-article1.component';

describe('EditorArticle1Component', () => {
  let component: EditorArticle1Component;
  let fixture: ComponentFixture<EditorArticle1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorArticle1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorArticle1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
