import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Article} from '../../../models/article';
import {ArticleService} from '../../../services/article.service';
import { FileUploader } from 'ng2-file-upload';
import {Media} from '../../../models/media';
import {MediaService} from '../../../services/media.service';
import {MediaArticle} from '../../../models/media-article';
import {MediaArticlePk} from '../../../models/media-article-pk';
import {ActivatedRoute, Route, Router} from '@angular/router';
import {TagService} from '../../../services/tag.service';
const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';
@Component({
  selector: 'app-editor-article1',
  templateUrl: './editor-article1.component.html',
  styleUrls: ['./editor-article1.component.scss'],
  providers: [ArticleService, MediaService, TagService]
})
export class EditorArticle1Component implements OnInit, OnChanges {
  public uploader: FileUploader = new FileUploader({url: URL});
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;
  titreAr: string;
  public fileOverBase (e: any): void {
    this.hasBaseDropZoneOver = e;
  }
  public fileOverAnother (e: any): void {
    this.hasAnotherDropZoneOver = e;
  }
  formArticle: FormGroup;
  @Output() back: EventEmitter<Article> = new EventEmitter();
  @Input() a: Article;
  @Input() defaultContentAr: string;
  m: Media;
  mediaOkk: boolean = false;
  media: Media;
  message: string;
  articles: Article[];
  medias: Media[];
  article: Article;
  feedBack (ar: Article): void {
    this.articles.push(ar);
  }
  ngOnInit (): void {
    this.items = new Array<string>();
    this.tagService.getTags().subscribe(
      tags => {
        for(let i = 0; i < tags.length; i++){
          this.items[i] = tags[i].nom;
          console.log(this.items[i]);
        }
      },
      error => {console.log(error);}
    );
    this.formArticle = this.fB.group({
      'titre': ['', Validators.required],
    });
    if(this.a === undefined){
      console.log('*******************************************');
      this.a = new Article();
      this.a.corps = 'Pour Créer';
      this.a.mediaArticles = [];
      this.titreAr = 'Nouvel article';
    }else {
      this.titreAr = 'Modification';
    }
    this.defaultContentAr = this.a.corps;
    this.medias = [];
    this.m = new Media();
    //
  }

  onContentChange (event: string) {
    this.a.corps = event;
    console.log(this.a.corps);
  }

  constructor (private fB: FormBuilder,
               private router: Router,
               private route: ActivatedRoute,
               private articleService: ArticleService,
               private mediaservice: MediaService,
               private tagService: TagService) {

  }
  public fileDropped (event: any) {
    const $this = this;
    for (let item of event) {
      const media = new Media;
      let reader = new FileReader();
      console.log(item);
      reader.readAsDataURL(item);
      reader.onload = function () {
        media.nom = item.name;
        media.url = reader.result.split(',')[1];
        media.type = item.name.split('.')[1];
        $this.medias.push(media);
      };
    }
    console.log(this.medias);
  }
  onUpload () {
    console.log('#############');
    this.mediaservice.createMedia(this.medias).subscribe(
      medias => {
        console.log('************************');
        console.log(medias);
        this.message = 'Médias créés avec succès!';
        for (let m of medias) {
          const mediaArticle = new MediaArticle();
          mediaArticle.pk = new MediaArticlePk();
          mediaArticle.pk.media = m;
          this.a.mediaArticles.push(mediaArticle);
        }
        this.mediaOkk = true;
      });
  }
  onSubmit () {
    if (this.a.id === undefined) {
      console.log(this.a.mediaArticles);
      this.a.state = false;
      this.a.pageCible = 'news';
      this.articleService.createArticle(this.a).subscribe(
        article => {
          this.back.emit(article);
          this.message = 'Créé avec succès!';
          this.mediaOkk = true;
          this.router.navigate(['pages/articles']);
          setTimeout(
            () => this.message = undefined,
            2000
          );
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.articleService.editArticle(this.a).subscribe(
        article => {
          this.back.emit(article);
          this.message = 'Mise à jour réussie!';
          this.router.navigate(['pages/articles']);
          setTimeout(
            () => this.message = undefined,
            2000
          );
        },
        error => {
          console.log(error);
        }
      );
    }
  }
  public items: Array<string>;
  public value: any = ['BIG DATA'];
  public _disabledV: string = '0';
  public disabled: boolean = false;
  public selected (value: any): void {
    console.log('Selected value is: ', value);
  }
  public removed (value: any): void {
    console.log('Removed value is: ', value);
  }
  public refreshValue (value: any): void {
    this.value = value;
  }
  public itemsToString (value: Array<any> = []): string {
    return value
      .map((item: any) => {
        return item.text;
      }).join(',');

  }
  /*
    getArticle (): void {
      this.articleService.getArticleById(parseInt(this.route.snapshot.paramMap.get('id'))).subscribe(
        a => {
          if (a === undefined) {
            console.log('*******************************************');
            this.a = new Article();
            this.a.corps = 'Pour Créer';
            this.a.mediaArticles = [];
            this.titreAr = 'Nouvel article';
          }
          else {
            console.log('#######################');
            console.log(a.corps);
            this.a = a;
            this.a.corps = a.corps;
            this.a.mediaArticles = a.mediaArticles;
            this.titreAr = 'Modification';
          }
        },
        error => {
          console.log(error);
        }
      );
    }
    */
  ngOnChanges (changes: SimpleChanges): void {
    if (this.a && this.a.corps) {
      this.defaultContentAr = this.a.corps;
    }
  }
}

