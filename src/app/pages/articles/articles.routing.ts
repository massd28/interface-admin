import { Routes, RouterModule } from '@angular/router';
import {ReadArticleComponent} from './../read-article/read-article.component';
import {EditorArticleComponent} from '../editor-article/editor-article.component';
import {EditorAppelDoffresComponent} from '../editor/components/editor-appel-doffres/editor-appel-doffres.component';
import {EditorEventsComponent} from '../events/editor-events/editor-events.component';
import {AppelDOffresComponent} from '../appel-doffres/appel-doffres.component';
import {ShowArticleComponent} from '../show-article/show-article.component';
import {ArticlesComponent} from './articles.component';
import {PrevComponent} from '../prev/prev.component';


const childRoutes: Routes = [
  {
    path: '',
    component: ArticlesComponent,

  },
  {path:'showarticle/:id', component: ShowArticleComponent,},
  {path:'prev/:id', component: PrevComponent,},


  {path:'editor-article', component: EditorArticleComponent},

  {path:'annonces', component: AppelDOffresComponent},
];

export const routing = RouterModule.forChild(childRoutes);


