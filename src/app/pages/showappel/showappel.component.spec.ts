import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowappelComponent } from './showappel.component';

describe('ShowappelComponent', () => {
  let component: ShowappelComponent;
  let fixture: ComponentFixture<ShowappelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowappelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowappelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
