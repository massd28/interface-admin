 import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Article} from '../../models/article';
import {AppelDOffresService} from '../../services/appel-d-offres.service';
import {Observable} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {AppelDOffres} from '../../models/appel-d-offres';

@Component({
  selector: 'app-showappel',
  templateUrl: './showappel.component.html',
  styleUrls: ['./showappel.component.scss'],
  providers: [AppelDOffresService]
})

export class ShowappelComponent implements OnInit {


  @Output() back: EventEmitter<AppelDOffres> = new EventEmitter();
  appel: AppelDOffres;
  idd: number;
  htmlStr: string = '<strong>The Tortoise</strong> &amp; the Hare';
  modifAppel = false;
  constructor(private appelDOffresService: AppelDOffresService,
              private router: Router,
              private route: ActivatedRoute) {}
  ngOnInit(): void {
    this.idd = this.route.snapshot.params['id'];
    this.getAppelDOffres();

  }


  onPublier(a: AppelDOffres) {
    a.state = true;
    a.datePublication = new Date();
    this.appelDOffresService.editAppelDOffres(a).subscribe(
      appel => {
        this.back.emit(appel);

        this.router.navigate(['pages/annonces']);
        console.log(a.state);

      }
    );
  }

  getAppelDOffres(): void {
    this.appelDOffresService.getAppelDOffresById(this.route.snapshot.params['id']).subscribe(
      a => {
        a.images = [];
        a.videos = [];
        a.documents = [];
        this.appelDOffresService.getMediasByAppelDOffres(a.id).subscribe(
          medias => {
            for (let i = 0; i < medias.length; i++) {
              if (medias[i].type === 'png' || medias[i].type === 'jpg' || medias[i].type === 'jpeg') {
                a.images.push(medias[i]);
              }
              if (medias[i].type === 'mp4' || medias[i].type === 'mov' || medias[i].type === 'wav') {
                a.videos.push(medias[i]);
              }
              if (medias[i].type === 'pdf' || medias[i].type === 'txt') {
                a.documents.push(medias[i]);
              }
            }
          }
        );
        this.appel = a;
        document.getElementById('texte').innerHTML = a.corps;

      }
    );
  }
}
