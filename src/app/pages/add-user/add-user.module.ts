import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ModalModule} from 'ngx-modal';
import {NgxPaginationModule} from 'ngx-pagination';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../shared/shared.module';
import {routing} from './add-user.routing';
import {RouterModule} from '@angular/router';




@NgModule({
  declarations: [


  ],
  imports: [
    CommonModule,
    SharedModule,
    ModalModule,
     routing,
    RouterModule,
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,

  ]
})
export class AddUserModule { }

