import { Routes, RouterModule } from '@angular/router';
import {AddUserComponent} from './add-user.component';


const childRoutes: Routes = [
  {
    path: '',
    component: AddUserComponent,

  },

];

export const routing = RouterModule.forChild(childRoutes);
