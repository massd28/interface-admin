import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../../models/user';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
  providers: [UserService]
})
export class AddUserComponent implements OnInit {

  formUser: FormGroup;
  @Output()back: EventEmitter<User> = new EventEmitter();
  u: User;
  message: string;
  users: User[];
  user: User;
  feedBack(us: User): void {
    this.users.push(us);
  }
  ngOnInit(): void {
    this.user = new User();
    /*this.users = this.userService.users;
    this.getUsers();*/

  }

  constructor(private fB: FormBuilder,
              private userService: UserService,
              private router: Router
  ) {
    this.formUser = this.fB.group({
      'prenom': ['', Validators.required],
      'nom': ['', Validators.required],
      'mail': ['', Validators.required],
      'telephone': ['', Validators.required],
      'password': ['', Validators.required],
      'username': ['', Validators.required],
      'role': ['', Validators.required],
    });
    this.u = new User();
  }

  onSubmit() {
    if (this.u.id === undefined) {
      this.userService.createUser(this.u).subscribe(
        user => {
          this.back.emit(user);
          this.message = 'Successfully created!';
          this.router.navigate(['pages/index']);
          setTimeout(
            () => this.message = undefined,
            2000
          );
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.userService.editUser(this.u).subscribe(
        user => {
          this.back.emit(user);
          this.message = 'Successfully updated!';
          this.router.navigate(['pages/index']);
          setTimeout(
            () => this.message = undefined,
            2000
          );
        },
        error => {
          console.log(error);
        }
      );
    }
  }


}


