import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Filiere} from '../../../../models/filiere';
import {FiliereService} from '../../../../services/filiere.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DepartementService} from '../../../../services/departement.service';
import {Departement} from '../../../../models/departement';
import { Router} from '@angular/router';

@Component({
  selector: 'app-ajout-filiere',
  templateUrl: './ajout-filiere.component.html',
  styleUrls: ['./ajout-filiere.component.scss'],
  providers: [FiliereService, DepartementService]
})
export class AjoutFiliereComponent implements OnInit, OnChanges {
  @Output()backFil: EventEmitter<Filiere> = new EventEmitter();
  formFil: FormGroup;
  @Input() f: Filiere;
  @Input() d: Departement;
  @Input()contentPres: string;
  @Input()contentObj: string;
  @Input()contentCond: string;
  @Input()contentDeb: string;
  messageFil: any;
  cardTitle: string;
  submittingForm: boolean;
  showPresentation = false;
  showObjectifs = false;
  showConditions = false;
  showDebouches = false;
  constructor( private filiereService: FiliereService,
               private router: Router,
               private fB: FormBuilder) {
    this.formFil = this.fB.group(
      {
        'nomFil': ['', Validators.required],
      }
    );
  }

  ngOnInit() {
    if (this.f.id === undefined){
      this.f = new Filiere();
      this.f.presentation = '';
      this.f.objectif = '';
      this.f.conditionAdmission = '';
      this.f.debouches = '';
      this.cardTitle = 'Nouvelle filière';
    }else{
      this.cardTitle = 'Modification';
    }
    this.contentPres = this.f.presentation;
    this.contentObj = this.f.objectif;
    this.contentCond = this.f.conditionAdmission;
    this.contentDeb = this.f.debouches;
  }
  onSubmitFil() {
    this.submittingForm = true;
    if (this.f.id === undefined) {
      this.f.departement = this.d;
      this.filiereService.createFiliere(this.f).subscribe(
        fil => {
          this.backFil.emit(fil);
          this.submittingForm = false;
          this.messageFil = 'Créée avec succès!';
          setTimeout(
            () => this.messageFil = undefined,
            2000
          );
          this.router.navigate(['pages/departements/details/',this.f.departement.id]);
          },
        error => {
          console.log(error);
        }
      );
    } else {
      this.f.departement = this.d;
      this.filiereService.editFiliere(this.f).subscribe(
        fil => {
          this.backFil.emit(fil);
          this.submittingForm = false;
          this.messageFil = 'Mise à jour réussie!';
          setTimeout(
            () => this.messageFil = undefined,
            2000
          );
          this.router.navigate(['pages/departements/details/',this.f.departement.id]);
          },
        error => {
          console.log(error);
        }
      );
    }
  }

  onPresentationChange (event1: string) {
    this.f.presentation = event1;
    console.log(this.f.presentation);
  }

  onObjectifsChange (event2: string) {
    this.f.objectif = event2;
    console.log(this.f.objectif);
  }

  onConditionsChange (event3: string) {
    this.f.conditionAdmission = event3;
    console.log(this.f.conditionAdmission);
  }

  onDebouchesChange (event4: string) {
    this.f.debouches = event4;
    console.log(this.f.debouches);
  }

  ngOnChanges (changes: SimpleChanges): void {
    if(this.f && this.f.presentation){
      this.contentPres = this.f.presentation;
    }
    if (this.f && this.f.objectif){
      this.contentObj = this.f.objectif;
    }
    if(this.f && this.f.conditionAdmission){
      this.contentCond = this.f.conditionAdmission;
    }
    if(this.f && this.f.debouches){
      this.contentDeb = this.f.debouches;
    }
  }

  onShowPresentation () {
    this.showPresentation = true;
    this.showConditions = this.showDebouches = this.showObjectifs = false;
  }

  onShowObjectifs () {
    this.showObjectifs = true;
    this.showConditions = this.showDebouches = this.showPresentation = false;
  }

  onShowConditions () {
    this.showConditions = true;
    this.showPresentation = this.showDebouches = this.showObjectifs = false;
  }

  onShowDebouches () {
    this.showDebouches = true;
    this.showConditions = this.showPresentation = this.showObjectifs = false;
  }
}
