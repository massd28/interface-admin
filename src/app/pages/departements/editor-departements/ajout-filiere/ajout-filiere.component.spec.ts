import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutFiliereComponent } from './ajout-filiere.component';

describe('AjoutFiliereComponent', () => {
  let component: AjoutFiliereComponent;
  let fixture: ComponentFixture<AjoutFiliereComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjoutFiliereComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutFiliereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
