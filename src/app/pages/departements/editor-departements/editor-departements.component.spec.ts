import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorDepartementsComponent } from './editor-departements.component';

describe('EditorDepartementsComponent', () => {
  let component: EditorDepartementsComponent;
  let fixture: ComponentFixture<EditorDepartementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorDepartementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorDepartementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
