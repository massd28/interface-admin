import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Departement} from '../../../models/departement';
import {DepartementService} from '../../../services/departement.service';
import {Router} from '@angular/router';
import {Media} from '../../../models/media';
import {MediaService} from '../../../services/media.service';
import {FileUploader} from 'ng2-file-upload';
const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
  selector: 'app-editor-departements',
  templateUrl: './editor-departements.component.html',
  styleUrls: ['./editor-departements.component.scss'],
  providers: [DepartementService, MediaService]
})
export class EditorDepartementsComponent implements OnInit, OnChanges {
  public uploader: FileUploader = new FileUploader({url: URL});
  formDept: FormGroup;
  @Input() d: Departement;
  @Input()defaultContent: string;
  @Output()back: EventEmitter<Departement> = new EventEmitter();
  message: string;
  chaine: string;
  medias: Media[];
  media: Media;
  submittingForm: boolean;
  // @Input()isOk;
  ngOnInit(): void {
    this.formDept = this.fB.group({
      'nom': ['', Validators.required],
    });
    //this.d = new Departement();//A COMMENTER OU.... PAS
    if(this.d === undefined){
      this.d = new Departement();
      this.d.description = '';
      this.d.medias = [];
      this.chaine ='Nouveau département';
    }
    else {
     this.chaine = 'Modification';
    }
    this.defaultContent = this.d.description;
    this.media = new Media();
    this.medias = [];
    document.getElementById('text-output').innerHTML = this.d.description;
    //this.isOk = true;
  }

  onContentChange(event: string) {
    this.d.description = event;
    console.log(this.d.description);
  }
  constructor(private fB: FormBuilder,
              private departementService: DepartementService,
              private mediaservice: MediaService,
              private router: Router) {
  }
  public fileDropped(event: any) {

    const $this = this;
    for(let item of event){
      const media = new Media;
      let reader = new FileReader();
      console.log(item);
      reader.readAsDataURL(item);
      reader.onload = function () {
        media.nom = item.name;
        //recupérer base 64 dans media.url
        media.url = reader.result.split(',')[1];
        media.type = item.name.split('.')[item.name.split('.').length - 1];
        $this.medias.push(media);
      };
    }
    console.log(this.medias);
  }
  /*
  public fileDropped(event: any) {
    const $this = this;
    for(let item of event){
      const media = new Media;
      let reader = new FileReader();
      console.log(item);
      reader.readAsDataURL(item);
      reader.onload = function () {
        media.nom = item.name;
        media.url = reader.result.split(',')[1];
        media.type = item.name.split('.')[1];
        $this.medias.push(media);
      };
    }
  }*/
  onUpload() {
    this.submittingForm = true;
    console.log('############# AVANT POST');
    console.log(this.medias);
    this.mediaservice.createMedia(this.medias).subscribe(
      medias => {
        console.log('************************');
        console.log(medias);
        this.medias = medias;
        this.submittingForm = false;
        this.message = 'Médias chargés avec succès';
        setTimeout(
          () => this.message = undefined,
          2000
        );
        /**for(let m of medias){
          this.d.medias.push(m);
        }
        console.log('@@@@@');
        console.log(medias);*/
      },err => {console.log(err);});
  }
    onSubmit() {
    this.submittingForm = true;
    if (this.d.id === undefined) {
      this.d.medias = this.medias;
      console.log('just après submitt');
      this.departementService.createDepartement(this.d).subscribe(
        dept => {
          this.back.emit(dept);
          this.d = dept;
          this.submittingForm = false;
          this.message = 'Créé avec succès!';
          setTimeout(
            () => this.message = undefined,
            2000
          );
          this.router.navigate(['pages/departements/list-departements']);
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.d.medias = this.medias;
      this.departementService.editDepartement(this.d).subscribe(
        dept => {
          this.back.emit(dept);
          this.submittingForm = false;
          this.message = 'Mise à jour réussie!';
          setTimeout(
            () => this.message = undefined,
            2000
          );
          this.router.navigate(['pages/departements/list-departements']);
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  ngOnChanges (changes: SimpleChanges): void {
    if(this.d && this.d.description){
      this.defaultContent = this.d.description;
    }
  }
}
