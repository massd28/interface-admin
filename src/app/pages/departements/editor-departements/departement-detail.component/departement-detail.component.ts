import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Departement} from '../../../../models/departement';
import {DepartementService} from '../../../../services/departement.service';
import {Filiere} from '../../../../models/filiere';
import {FiliereService} from '../../../../services/filiere.service';

@Component({
  selector: 'app-departement-detail',
  templateUrl: './departement-detail.component.html',
  styleUrls: ['./departement-detail.component.scss'],
  providers: [DepartementService, FiliereService]
})
export class DepartementDetailComponent implements OnInit {
  dept: Departement;
  idDept: number;
  ajout = false;
  liste = false;
  filiere: Filiere;
  mel: any;
  modifDept = false;
  descriptiondd: string;
  gettingFilieres: boolean;
  deletingFiliere: boolean;
  deletingDept: boolean;
  constructor(private route: ActivatedRoute,
              private router: Router,
              private deptService: DepartementService,
              private filService: FiliereService) { }

  retourCreation(f: Filiere): void {
    this.dept.filieres = [];
    //ERRREUR
    this.dept.filieres.push(f);
    this.filiere = new Filiere();
    this.filiere.description = '';
    this.ajout = false;
  }
  ngOnInit() {
    this.dept = new Departement();
    this.dept.medias = [];
    let id = parseInt(this.route.snapshot.paramMap.get('id'));
    this.idDept = id;
    this.getDept(this.idDept);
    this.filiere = new Filiere();
  }
   getDept(id: number) {
    this.deptService.getDepartementById(id).subscribe(
      d => {
        d.medias = [];
        this.deptService.getMediasByDepartement(d.id).subscribe(
          medias => {
            for(let m of medias){
              d.medias.push(m);
            }
          }
        );
        this.dept = d;
        this.getFilieres(this.dept);
        this.descriptiondd = this.dept.description;
        console.log(this.dept.medias);
      },
      error => {
        console.log(error);
      }
    );
  }
  onAjoutFiliere() {
    this.ajout = true;
    this.liste = false;
    this.modifDept = false;
    this.filiere = new Filiere();
    this.filiere.description ='';
  }

  onVoirFilieres() {
    this.liste = true;
    this.ajout = false;
    this.modifDept = false;
    //this.getFilieres(this.dept);
    console.log('After getFilieres()');
  }

  onSelectFil(fil: Filiere) {
    this.router.navigate(['pages/departements/filieres', fil.id]);
  }

  onDeleteFil(fil: Filiere) {
    if(confirm('Etes-vous sur ?')){
      this.deletingFiliere = true;
      this.filService.deleteFiliere(fil).subscribe(
        () => {
          for (let i = 0; i < this.dept.filieres.length; i++){
            if (this.dept.filieres[i].id === fil.id){
              this.dept.filieres.splice(i, 1);
              break;
            }
          }
          this.deletingFiliere = false;
          this.mel = 'Supprimée avec succès!';
          setTimeout(
            () => this.mel = undefined,
            2000
          );
          this.router.navigate(['pages/departements/details'], fil.departement.id);
        },
        error1 => {console.log(error1);}
      );
    }
  }

  getFilieres(dept: Departement) {
    this.gettingFilieres = true
    this.deptService.getFilieresByDepartement(dept.id).subscribe(
      filieres => {
        this.dept.filieres = filieres;
        console.log(this.dept.filieres);
        this.gettingFilieres = false;
      },
      error => {
        console.log(error);
      }
    );

  }
  /*
  getMedias(dept: Departement) {
    this.deptService.getMediasByDepartement(dept.id).subscribe(
      medias => {
        this.dept.medias = medias;
        console.log(this.dept.medias);
        
      },
      error => {
        console.log(error);
      }
    );

  }
*/
  onModifierDept () {
    this.ajout = false;
    this.liste = false;
    this.getDept( parseInt(this.route.snapshot.paramMap.get('id')));
   // this.dept.description = this.descriptiondd;
    console.log(this.descriptiondd);
    console.log('***********************************************');
    console.log(this.dept.description);
    this.modifDept = true;
  }

  onSuppDept () {
    this.deletingDept = true;
    this.deptService.deleteDepartement(this.dept).subscribe(
      ()=>{
        this.deletingDept = false;
        this.mel = 'Supprimé avec succès!';
        console.log(this.mel);
        setTimeout(
          () => this.mel = undefined,
          2000
        );
        this.router.navigate(['pages/departements/list-departements']);
      },
      error => {
        console.log(error);
      }
    )
  }
}
