import { Component, OnInit } from '@angular/core';
import {FiliereService} from '../../../../../services/filiere.service';
import {Filiere} from '../../../../../models/filiere';
import {ActivatedRoute, Router} from '@angular/router';
import {Departement} from '../../../../../models/departement';

@Component({
  selector: 'app-filiere-detail',
  templateUrl: './filiere-detail.component.html',
  styleUrls: ['./filiere-detail.component.scss'],
  providers: [FiliereService]
})
export class FiliereDetailComponent implements OnInit {
  fil: Filiere;
  idFil: number;
  modif = false;
  mess: any;
  dept: Departement;
  deletingFil: boolean;

  constructor(private route: ActivatedRoute,
              private filService: FiliereService,
              private router: Router) { }

  ngOnInit() {
    let id = parseInt(this.route.snapshot.paramMap.get('id'));
    this.idFil = id;
    this.getFil(this.idFil);
//  document.getElementById('text-output').innerHTML = this.fil.description;
  }

  private getFil(idFil: number) {
    this.filService.getFiliereById(idFil).subscribe(
      f => {
        this.fil = f;
        console.log(this.fil);
        document.getElementById('texte').innerHTML = this.fil.description;
      },
      error => {
        console.log(error);
      }
    );
  }

  onModifier(fil: Filiere) {
    this.getFil(parseInt(this.route.snapshot.paramMap.get('id')));
    this.modif = true;
    this.dept = fil.departement;
  }

  onSupp(fil: Filiere) {
    this.deletingFil = true;
    this.filService.deleteFiliere(fil).subscribe(
      () => {
        this.deletingFil = false;
        this.mess = 'Supprimée avec succès!';
        console.log(this.mess);
        setTimeout(
          () => this.mess = undefined,
          2000
        );
        this.router.navigate(['pages/departements/detail/',fil.departement.id]);
        },
      error => {
        console.log(error);
      }
    );
  }

  feedBackFil (f: Filiere) {
    this.dept.filieres.push(f);
  }
}
