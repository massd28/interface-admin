import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DepartementsComponent } from './departements.component';
import { routing } from './departements.routing';
import { SharedModule } from '../../shared/shared.module';
import {ModalModule} from 'ngx-modal';
import {NgxPaginationModule} from 'ngx-pagination';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EditorDepartementsComponent} from './editor-departements/editor-departements.component';
import {ListDepartementsComponent} from './list-departements/list-departements.component';
import {DepartementDetailComponent} from './editor-departements/departement-detail.component/departement-detail.component';
import {FiliereDetailComponent} from './editor-departements/departement-detail.component/filiere-detail/filiere-detail.component';
import {AjoutFiliereComponent} from './editor-departements/ajout-filiere/ajout-filiere.component';
import {NgxSmartModalModule, NgxSmartModalService} from 'ngx-smart-modal';
import {FileUploadModule} from 'ng2-file-upload';
import {SelectModule} from 'ng2-select';
import {Ng2SearchPipeModule} from 'ng2-search-filter';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ModalModule,
    routing,
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSmartModalModule,
    FileUploadModule,
    SelectModule,
    Ng2SearchPipeModule
  ],
  declarations: [
    DepartementsComponent,
    EditorDepartementsComponent,
    ListDepartementsComponent,
    DepartementDetailComponent,
    FiliereDetailComponent,
    AjoutFiliereComponent
  ],
  providers: [
    NgxSmartModalService
  ]
})
export class DepartementsModule { }
