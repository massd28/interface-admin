import {RouterModule, Routes} from '@angular/router';
import {DepartementsComponent} from './departements.component';
import {EditorDepartementsComponent} from './editor-departements/editor-departements.component';
import {ListDepartementsComponent} from './list-departements/list-departements.component';
import {DepartementDetailComponent} from './editor-departements/departement-detail.component/departement-detail.component';
import {FiliereDetailComponent} from './editor-departements/departement-detail.component/filiere-detail/filiere-detail.component';

const childRoutes: Routes = [
  {
    path: '',
    component: DepartementsComponent
  },
  {path: '', redirectTo: 'list-departements', pathMatch: 'full'},
  {path:'list-departements', component: ListDepartementsComponent},
  {path:'editor-departement', component: EditorDepartementsComponent},
  {path: 'details/:id', component: DepartementDetailComponent},
  {path: 'filieres/:id', component: FiliereDetailComponent},
];


export const routing = RouterModule.forChild(childRoutes);
