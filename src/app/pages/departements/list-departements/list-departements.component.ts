import { Component, OnInit } from '@angular/core';
import {DepartementService} from '../../../services/departement.service';
import {Departement} from '../../../models/departement';
import {Router} from '@angular/router';
import swal from "sweetalert2";

@Component({
  selector: 'app-list-departements',
  templateUrl: './list-departements.component.html',
  styleUrls: ['./list-departements.component.scss'],
  providers: [DepartementService]
})
export class ListDepartementsComponent implements OnInit {
  depts: Departement[];
  alerte: string;
  gettingDepts: boolean;
  deleting: boolean;
  term0: string;
  constructor(private departementService: DepartementService,
              private router: Router) { }

  ngOnInit() {
    this.term0 = '';
    this.getDepts();

  }

  private getDepts() {
    this.gettingDepts = true;
    this.departementService.getDepartements().subscribe(
      depts => {
        this.depts = depts;
        console.log(this.depts);
        this.gettingDepts = false;
      },
      error => {
        console.log(error);
      }
    );
  }
  onDeleteDept(dept: Departement): void {
    this.deleting = true;
    this.departementService.deleteDepartement(dept).subscribe(
      () => {
        for (let i = 0; i < this.depts.length; i++) {
          if (this.depts[i].id === dept.id) {
            this.depts.splice(i, 1);
            break;
          }
        }
        this.deleting = false;
        this.alerte = 'Supprimée avec succès!';
        setTimeout(
          () => this.alerte = undefined,
          2000
        );
      },
      error1 => {console.log(error1);}
    );
    this.router.navigate(['pages/departements/list-departements']);
  }

  onSelect(dept: Departement) {
    this.router.navigate(['pages/departements/details', dept.id]);
  }

  onAddDept(): void {
   this.router.navigate(['pages/departements/editor-departement']);
  }

}
