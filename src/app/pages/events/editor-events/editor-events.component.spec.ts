import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorEventsComponent } from './editor-events.component';

describe('EditorEventsComponent', () => {
  let component: EditorEventsComponent;
  let fixture: ComponentFixture<EditorEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
