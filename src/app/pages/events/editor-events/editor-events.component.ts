import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {EventService} from '../../../services/event.service';
import { FileUploader } from 'ng2-file-upload';
import {Media} from '../../../models/media';
import {Event} from '../../../models/event';
import {Router} from '@angular/router';
import {MediaService} from '../../../services/media.service';
import {MediaArticle} from '../../../models/media-article';
import {MediaArticlePk} from '../../../models/media-article-pk';
import {TagService} from '../../../services/tag.service';
import {Tag} from '../../../models/tag';
import {ArticleTag} from '../../../models/article-tag';
import {ArticleTagPk} from '../../../models/article-tag-pk';
import {Article} from '../../../models/article';
import {ArticleService} from '../../../services/article.service';

const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
  selector: 'app-editor-events',
  templateUrl: './editor-events.component.html',
  styleUrls: ['./editor-events.component.scss'],
  providers: [EventService, MediaService, TagService, ArticleService]
})
export class EditorEventsComponent implements OnInit {
  public uploader: FileUploader = new FileUploader({url: URL});
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;
  titre: string;

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }


  formEvent: FormGroup;
  @Output() back: EventEmitter<Event> = new EventEmitter();
  @Input() e: Event;
  m: Media;
  mediaOkk: boolean = false;
  media: Media;
  message: string;
  events: Event[];
  medias: Media[];
  event: Event;
  public items: Array<any>;
  tags: Tag[];
  ajoutTag: boolean = false;
  formTag: FormGroup;
  @Output() backTag: EventEmitter<Tag> = new EventEmitter();
  t: Tag;
  @Input() pellEvent: string;
  submitting: boolean;
  deleting: boolean;
  fr = {
    firstDayOfWeek: 0,
    dayNames: ['Dimanche', 'Lundi', 'Mardi','Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
    dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
    monthNames: [ 'Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Decembre' ],
    monthNamesShort: [ 'Jan', 'Fév', 'Mars', 'Avr', 'Mai', 'Juin','Juil', 'Août', 'Sep', 'Oct', 'Nov', 'Dec' ]
  };

  feedBack(ev: Event): void {
    this.events.push(ev);
  }

  ngOnInit(): void {
    this.event = new Event();

    this.event.corps = '';
    this.events = this.eventService.events;
    this.getEvents();
    console.log('After getArticles()');
    this.getTags();


  }
  onSubmitTag() {
    this.submitting = true;
    if (this.t.id === undefined) {
      this.tagService.createTag(this.t).subscribe(
        tag => {
          this.backTag.emit(tag);

          this.ajoutTag = false;
          const item = {
            id: tag.id,
            text: tag.nom
          };
          this.value.push(item);
          this.submitting = false;
        },
        error => {
          console.log(error);
        }
      );

    } else {
      this.tagService.editTag(this.t).subscribe(
        tag => {
          this.backTag.emit(tag);
          this.submitting = false;
        },
        error => {
          console.log(error);
        }
      );

    }
  }

  private getTags() {
    this.tagService.getTags().subscribe(
      tags => {
        this.tags = tags;
        for (let t of this.tags){
          const item = {
            id: t.id,
            text: t.nom
          }
          this.items.push(item) ;
        }
        console.log(this.items);
      },
      error => {
        console.log(error);

      }
    );
  }

  onContentChange(event: string) {
    this.e.corps = event;
    console.log(this.e.corps);
  }

  constructor(private fB: FormBuilder,
              private router: Router,
              private eventService: EventService,
              private mediaservice: MediaService,
              private tagService: TagService,
              private articleService: ArticleService) {
    this.formEvent = this.fB.group({
      'titre': ['', Validators.required],
      'dateDebut': ['', Validators.required],
      'dateFin': ['', Validators.required],

    });
    this.formTag = this.fB.group({
      'nom': ['', Validators.required],

    });
    this.t = new Tag();
    this.value = [];
    if(this.e === undefined){
      this.e = new Event();
      this.e.corps = '';
      this.e.articleTags = [];
      this.e.mediaArticles = [];
      this.titre = 'Nouvel évènement';
    } else {
      this.titre = 'Modification';
    }
    this.items = [];
    this.medias = [];
    this.m = new Media();
    this.pellEvent = this.e.corps;

  }

  ajouterTag(){
    this.ajoutTag = true;
  }
  public fileDropped(event: any) {

    const $this = this;
    for(let item of event){
      const media = new Media;
      let reader = new FileReader();
      console.log(item);
      reader.readAsDataURL(item);
      reader.onload = function () {
        media.nom = item.name;
        media.url = reader.result.split(',')[1];
        media.type = item.name.split('.')[item.name.split('.').length - 1];
        $this.medias.push(media);
      };
    }
    console.log(this.medias);
  }

  onUpload() {
    this.submitting = true;
    console.log('#############');
    this.mediaservice.createMedia(this.medias).subscribe(
      medias => {
        console.log('************************');
        console.log(medias);
        this.submitting = false;
        this.message = 'Médias chargés avec succès!';
        for(let m of medias){
          const mediaArticle = new MediaArticle();
          mediaArticle.pk = new MediaArticlePk();
          mediaArticle.pk.media = m;
          this.e.mediaArticles.push(mediaArticle);
        }
        this.mediaOkk = true;
      });


  }


  onSubmit() {
    this.submitting = true;
    if (this.e.id === undefined) {
      console.log(this.e.mediaArticles);
      for(let v of this.value){
        const articleTag = new ArticleTag();
        articleTag.pk = new ArticleTagPk();
        articleTag.pk.tag = v;
        this.e.articleTags.push(articleTag);
      }
      this.e.state = false;
      this.e.news = false;
      this.eventService.createEvent(this.e).subscribe(
        event => {
          this.back.emit(event);
          this.submitting = false;
          this.message = 'Evènement créé avec succès!';
          setTimeout(
            () => this.message = undefined,
            2000
          );
          this.mediaOkk = false;
          this.router.navigate(['pages/events/showevents/' + event.id]);
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.eventService.editEvent(this.e).subscribe(
        event => {
          this.back.emit(event);
          this.submitting = false;
          this.message = 'Mise à jour réussie!';
          setTimeout(
            () => this.message = undefined,
            2000
          );
          this.router.navigate(['pages/events/showevents/' + event.id]);
        },
        error => {
          console.log(error);
        }
      );

    }
  }

  private getEvents() {
    this.eventService.getEvents().subscribe(
      events => {
        this.events = events;
        console.log(this.events);
        for (let e of this.events){
          this.items.push(e.titre);
        }
      },
      error => {
        console.log(error);
      }
    );
  }




  public value: any = [''];
  public _disabledV: string = '0';
  public disabled: boolean = false;

  public selected(value: any): void {
    console.log('Selected value is: ', value);
  }

  public removed(value: any): void {
    console.log('Removed value is: ', value);
  }

  public refreshValue(value: any): void {
    this.value = value;
  }

  public itemsToString(value: Array<any> = []): string {
    return value
      .map((item: any) => {
        return item.text;
      }).join(',');
  }
  onSupp(media: Media, a: Article){
    this.deleting = true;
    this.articleService.deleteMedia(media, a).subscribe(
      () => {
        for (let i = 0; i < a.mediaArticles.length; i++) {
          if (a.mediaArticles[i].pk.media.id === media.id) {
            a.mediaArticles.splice(i, 1);
            break;
          }
        }
        this.articleService.editArticle(a).subscribe(
          a => { a = a;},
          error => {
            if (error.status === 500) {
              this.message = 'ERREUR SERVEUR';
            } else {
              this.message = 'ERREUR CLIENT';
            }
            console.log(error);
          }
        );
        this.deleting = false;
      },
      error => {
        console.log(error);
      }
    );
  }
}

