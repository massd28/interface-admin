import { Routes, RouterModule } from '@angular/router';
import { EventsComponent} from './events.component';
import {ShoweventsComponent} from '../showevents/showevents.component';
import {EditorEventsComponent} from './editor-events/editor-events.component';




const childRoutes: Routes = [
  {
    path: '',
    component: EventsComponent,

  },
  {
    path: 'showevents/:id',
    component: ShoweventsComponent
  },
  {
    path: 'editor-events',
    component: EditorEventsComponent
  }

];

export const routing = RouterModule.forChild(childRoutes);
