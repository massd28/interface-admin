import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Event} from '../../models/event';
import {EventService} from '../../services/event.service';
import {EditorComponent} from '../editor/editor.component';
import {Router} from '@angular/router';
import {Article} from '../../models/article';
import {ArticleService} from '../../services/article.service';
import {NgxSmartModalService} from 'ngx-smart-modal';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss'],
  providers: [EventService, NgxSmartModalService]
})
export class EventsComponent implements OnInit {

  formEvent: FormGroup;
  @Output()back: EventEmitter<Event> = new EventEmitter();
  e: Event;
  message: string;
  term: String;
  events: Event[];
  event: Event;
  deleting: boolean;


  /* pagination Info */
  pageSize = 10;
  pageNumber = 1;
  gettingEvents: boolean;
  feedBack(ev: Event): void {
    this.events.push(ev);
  }
  ngOnInit(): void {
    this.event = new Event();
    this.events = this.eventService.events;
    this.getEvents();
    /*document.getElementById('text-output').innerHTML = this.a.corps;*/
    console.log('After getArticles()');
    this.term = '';
  }
  onDeleteEvent(event: Event): void {
    this.deleting = true;
    this.eventService.deleteEvent(event).subscribe(
      () => {
        for (let i = 0; i < this.events.length; i++) {
          if (this.events[i].id === event.id) {
            this.events.splice(i, 1);
            break;
          }
        }
        this.deleting = false;
        this.router.navigate(['pages/events']);
      }
    );
  }
  constructor(private fB: FormBuilder,
              private eventService: EventService,
              private router: Router
  ) {}



  pageChanged(pN: number): void {
    this.pageNumber = pN;
  }
  getEvents() {
    this.gettingEvents = true;
    this.eventService.getEvents().subscribe(
      events => {
        this.events = events;
        console.log(this.events);
        this.gettingEvents = false;
      },
      error => {
        console.log(error);
      }
    );
  }

   goToEditorEvent(){
    this.router.navigate(['pages/events/editor-events']);
  }

  getState(a: Article){
    if (a.state === true){
      return "Publié";
    }
    if (a.state === false) {
      return "Brouillon";
    }
  }

  onVoirEvent(a: Event) {
    this.router.navigate(['pages/events/showevents/' + a.id]);
  }
}



