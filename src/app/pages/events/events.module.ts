import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ModalModule} from 'ngx-modal';
import {NgxPaginationModule} from 'ngx-pagination';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EventsComponent} from './events.component';
import {SharedModule} from '../../shared/shared.module';
import {routing} from './events.routing';
import {RouterModule} from '@angular/router';
import {FileUploadModule} from 'ng2-file-upload';
import {SelectModule} from 'ng2-select';
import {DateTimePickerModule} from 'ng-pick-datetime';
import {NgxSmartModalModule, NgxSmartModalService} from 'ngx-smart-modal';
import {ShoweventsComponent} from '../showevents/showevents.component';
//import {EditorEventsComponent} from './editor-events/editor-events.component';
import {EditorModule} from '../editor/editor.module';
import {EditorEventsComponent} from './editor-events/editor-events.component';
//import {EditorEventsComponent} from '../editor/components/editor-events/editor-events.component';
//import {EditorModule} from '../editor/editor.module';




@NgModule({
  declarations: [
    ShoweventsComponent,
    EditorEventsComponent,

  ],
  imports: [
    CommonModule,
    SharedModule,
    ModalModule,
    routing,
    RouterModule,
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    SelectModule,
    DateTimePickerModule,
    NgxSmartModalModule,
   // EditorModule

  ],
  providers: [NgxSmartModalService]
})
export class EventsModule { }
