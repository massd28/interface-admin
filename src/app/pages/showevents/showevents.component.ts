import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Event} from '../../models/event';
import {EventService} from '../../services/event.service';
import {Observable} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {Article} from '../../models/article';

@Component({
  selector: 'app-showevents',
  templateUrl: './showevents.component.html',
  styleUrls: ['./showevents.component.scss'],
  providers: [EventService]
})

export class ShoweventsComponent implements OnInit {

  @Output() back: EventEmitter<Event> = new EventEmitter();
  event: Event;
  idd: number;
  htmlStr: string = '<strong>The Tortoise</strong> &amp; the Hare';
  modifEvent = false;
  publierAfter: boolean;
  fr = {
    firstDayOfWeek: 0,
    dayNames: ['Dimanche', 'Lundi', 'Mardi','Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
    dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
    monthNames: [ 'Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Decembre' ],
    monthNamesShort: [ 'Jan', 'Fév', 'Mars', 'Avr', 'Mai', 'Juin','Juil', 'Août', 'Sep', 'Oct', 'Nov', 'Dec' ]
  };

  constructor(private eventService: EventService,
              private router: Router,
              private route: ActivatedRoute) {}
  ngOnInit(): void {
    this.idd = this.route.snapshot.params['id'];
    this.getEvents();

  }
  onPublerAfter(){
    this.publierAfter = true;
  }
  onValider(e: Event){
    e.state = true;
    e.datePublication = this.event.datePublication;
    this.eventService.editEvent(e).subscribe(
      article => {
        this.back.emit(article);

        this.router.navigate(['pages/events']);
        console.log(e.state);

      }
    );
  }

  onPublier(e: Event) {
    e.state = true;
    e.datePublication = new Date();
    this.eventService.editEvent(e).subscribe(
      event => {
        this.back.emit(event);

        this.router.navigate(['pages/events']);
        console.log(e.state);

      }
    );
  }

  getEvents(): void {
    this.eventService.getEventById(this.route.snapshot.params['id']).subscribe(
      a => {
        a.images = [];
        a.videos = [];
        a.documents = [];
        this.eventService.getMediasByEvent(a.id).subscribe(
          medias => {
            for (let i = 0; i < medias.length; i++) {
              if (medias[i].type === 'png' || medias[i].type === 'jpg' || medias[i].type === 'jpeg') {
                a.images.push(medias[i]);
              }
              if (medias[i].type === 'mp4' || medias[i].type === 'mov' || medias[i].type === 'wav') {
                a.videos.push(medias[i]);
              }
              if (medias[i].type === 'pdf' || medias[i].type === 'txt') {
                a.documents.push(medias[i]);
              }
            }
          }
        );
        this.event = a;
        document.getElementById('texte').innerHTML = a.corps;

      }
    );
  }
}
