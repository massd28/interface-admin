import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages/pages.component';
import {ReadArticleComponent} from './pages/read-article/read-article.component';
import {EditorArticleComponent} from './pages/editor-article/editor-article.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },




];

export const routing = RouterModule.forRoot(appRoutes);
